package com.concurrency.juc.c_004.exercise;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * 模拟多人买票
 */
public class ExerciseSell {
    private static final Logger log = LoggerFactory.getLogger(ExerciseSell.class);

    public static void main(String[] args) {
        int sellCount = 2000;
        //模拟多人买票
        TicketWindow window = new TicketWindow(1000);
        log.info("总票数: {}" , window.getTicket());
        // 所有线程的集合
        List<Thread> threadList = new ArrayList<>();
        // 卖出的票数统计
        List<Integer> soldList = new Vector<>();
        log.info("总人数: {}",sellCount);
        log.info("开始售票...");
        for (int i = 0; i < sellCount; i++) {
            Thread thread = new Thread(() -> {
                int amount = randomAmount();
                int sold = window.sell(amount);
                soldList.add(sold);

            });
            threadList.add(thread);
            thread.start();
        }
        //等待所有线程执行完毕
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                log.error("线程异常", e);
            }
        }
        // 统计卖出的票数和剩余票数
        log.info("余票: {}" , window.getTicket());
        log.info("卖出的票数:{} " , soldList.stream().mapToInt(Integer::intValue).sum());
    }

    // Random 为线程安全
    static Random random = new Random();

    //随机1-5
    public static int randomAmount(){
        return random.nextInt(5) + 1;
    }

}

/**
 * 售票窗口
 */
@Getter
class TicketWindow {
    //获取余票数量
    private int ticket;

    public TicketWindow(int ticket) {
        this.ticket = ticket;
    }

    //售票
    public synchronized int sell(int amount) {
        if (this.ticket >= amount) {
            this.ticket-=amount;
            return amount;
        } else {
            return 0;
        }
    }
}