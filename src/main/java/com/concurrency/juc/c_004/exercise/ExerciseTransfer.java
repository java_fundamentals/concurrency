package com.concurrency.juc.c_004.exercise;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

/**
 * 转账类
 */
public class ExerciseTransfer {
    private static final Logger log = LoggerFactory.getLogger(ExerciseTransfer.class);

    public static void main(String[] args) {
         Account account1 = new Account(1000);
         Account account2 = new Account(2000);
         Thread t1 = new Thread(() -> {
             for (int i = 0; i < 1000; i++) {
                 int amount = randomAmount();
                 log.info("account1 transfer {} to account2", amount);
                 account1.transfer(account2, amount);
             }
         },"t1");

        Thread t2 = new Thread(() -> {
             for (int i = 0; i < 1000; i++) {
                 int amount = randomAmount();
                 log.info("account2 transfer {} to account1", amount);
                 account2.transfer(account1, amount);
             }
         },"t2");
         //启动两个线程，模拟两个账户转账
        try {
            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        // 查看转账2000次后的总金额
        log.info("total:{}", (account1.getMoney()+account2.getMoney()));
    }
    //Ramdon 为线程安全
    static Random random = new Random();

    //随机1-100
    public static int randomAmount() {
        return random.nextInt(100) + 1;
    }
}

/**
 * 账户类
 */
@Setter
@Getter
class Account {
    private int money;

    public Account(int money) {
        this.money = money;
    }

    /**
     * 转账
     * @param target 目标账户
     * @param amount 转账金额
     */
    public void transfer(Account target, int amount) {
        synchronized (Account.class) {
            if (amount > 0 && amount <= this.money) {
                this.money -= amount;
                target.setMoney(target.getMoney() + amount);
            }
        }
    }
}