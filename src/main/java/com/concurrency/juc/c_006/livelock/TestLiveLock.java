package com.concurrency.juc.c_006.livelock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 活锁示例
 * 两个线程交替修改共享变量，但永远无法成功，因为两个线程都在等待对方修改，但永远无法成功。
 * 解决方法：引入超时机制，设置一个超时时间，超过超时时间后，线程放弃资源，重新竞争资源。
 */
public class TestLiveLock {
    private static final Logger log = LoggerFactory.getLogger(TestLiveLock.class);

    static volatile int count = 10;
    static final Object lock = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            while (count > 0) {
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                count--;
                log.info("t1: count = " + count);
            }
        }, "t1").start();

        new Thread(() -> {
            while (count < 20) {
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                count++;
                log.info("t2: count = " + count);
            }
        },"t2").start();
    }
}
