package com.concurrency.juc.c_006.deadlock.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 测试死锁
 */
public class TestDeadLock {
    private static final Logger log = LoggerFactory.getLogger(TestDeadLock.class);

    public static void main(String[] args) {
        Chopstick c1 = new Chopstick("1");
        Chopstick c2 = new Chopstick("2");
        Chopstick c3 = new Chopstick("3");
        Chopstick c4 = new Chopstick("4");
        Chopstick c5 = new Chopstick("5");
        new Philosopher("苏格拉底", c1, c2).start();
        new Philosopher("柏拉图", c2, c3).start();
        new Philosopher("亚里士多德", c3, c4).start();
        new Philosopher("狄德罗", c4, c5).start();
        new Philosopher("马克思", c5, c1).start();
    }
}

/**
 * 死锁的例子
 * 五个哲学家围着一张圆桌，每个哲学家手持一根筷子，思考问题。
 * 哲学家们的思考方式是互相抢夺对方的筷子，如果两个哲学家同时抢夺对方的筷子，就会发生死锁。
 * 为了避免死锁，哲学家们必须按照一定的顺序思考问题，并且每次只思考一个问题。
 * 为了避免死锁，我们可以让每个哲学家都按照编号的顺序思考问题，这样就不会发生死锁。
 */
class Philosopher extends Thread {
    private static final Logger log = LoggerFactory.getLogger(Philosopher.class);

    final Chopstick left;
    final Chopstick right;

    public Philosopher(String name, Chopstick left, Chopstick right) {
        super(name);
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (left) {
                synchronized (right) {
                    eat();
                }
            }
        }
    }

    public void eat() {
        log.info("正在吃饭：" + Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Chopstick {
    private final String name;

    public Chopstick(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "筷子{"+name+"}";
    }
}