package com.concurrency.juc.c_006.deadlock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * DeadLock: 两个线程互相持有对方需要的锁，导致它们都无法继续运行，称之为死锁。
 * 死锁的发生是因为两个线程都在等待对方持有的锁，而对方也在等待自己持有的锁，导致两个线程都无法继续运行。
 * 解决死锁的方法：
 * 1. 超时机制：设置一个超时时间，如果超过这个时间还不能获取到锁，则放弃获取锁，并释放自己持有的锁。
 * 2. 资源排序：按照资源的申请顺序排序，申请资源的线程一定先申请到资源，避免死锁。
 * 3. 主动释放锁：当线程发生死锁时，主动释放自己持有的锁，让对方获得锁，以便让线程继续运行。
 */
public class TestDeadLock {
    private static final Logger log = LoggerFactory.getLogger(TestDeadLock.class);

    public static void main(String[] args) {
        test1();
    }

    private static void test1(){
        Object A = new Object();
        Object B = new Object();

        Thread t1 = new Thread(() -> {
            synchronized (A) {
                log.info("lock A");
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (B) {
                    log.info("lock B");
                    log.info("操作...");
                }
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            synchronized (B) {
                log.info("lock B");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (A) {
                    log.info("lock A");
                    log.info("操作...");
                }
            }
        }, "t2");

        t1.start();
        t2.start();
    }
}
