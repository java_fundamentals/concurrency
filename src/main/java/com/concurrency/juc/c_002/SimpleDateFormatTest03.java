package com.concurrency.juc.c_002;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 通过Synchronized锁解决SimpleDateFormat类的线程安全问题
 * synchronized锁方式在处理问题，通过加锁的方式，使同一时刻只能有一个线程执行格式化日期
 * 和时间的操作。这种方式虽然减少了SimpleDateFormat对象的创建，但是由于同步锁的存在，导致性能下降，所以，不推荐在
 * 高并发要求的生产环境使用。
 * @author liuc
 * @date 2024-10-14 11:35
 */
public class SimpleDateFormatTest03 {
    private static final Logger log = LoggerFactory.getLogger(SimpleDateFormatTest03.class);
    //执行总次数
    private static final int EXECUTE_COUNT = 1000;
    //同时运行的线程数量
    private static final int THREAD_COUNT = 20;
    //SimpleDateFormat对象
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static void main(String[] args) throws InterruptedException {
        final Semaphore semaphore = new Semaphore(THREAD_COUNT);
        final CountDownLatch countDownLatch = new CountDownLatch(EXECUTE_COUNT);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < EXECUTE_COUNT; i++){
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    try {
                        synchronized (simpleDateFormat){
                            simpleDateFormat.parse("2020-01-01");
                        }
                    } catch (ParseException | NumberFormatException e) {
                        log.error("线程：" + Thread.currentThread().getName() + " 格式化日期失败");
                        log.error("异常信息：",e);
                        System.exit(1);
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
                    log.error("信号量发生错误");
                    log.error("异常信息：",e);
                    System.exit(1);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("所有线程格式化日期成功");
    }
}