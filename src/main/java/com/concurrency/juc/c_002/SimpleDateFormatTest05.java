package com.concurrency.juc.c_002;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 使用ThreadLocal存储每个线程拥有的SimpleDateFormat对象的副本，能够有效的避免多线程造成的线程安全问题。
 * ThreadLocal通过保存各个线程的SimpleDateFormat类对象的副本，使每个线程在运行时，各自使用自身绑定的
 * SimpleDateFormat对象，互不干扰，执行性能比较高，推荐在高并发的生产环境使用。
 * @author liuc
 * @date 2024-10-14 11:48
 */
public class SimpleDateFormatTest05 {
    private static final Logger log = LoggerFactory.getLogger(SimpleDateFormatTest05.class);
    //执行总次数
    private static final int EXECUTE_COUNT = 1000;
    //同时运行的线程数量
    private static final int THREAD_COUNT = 20;
    private static final ThreadLocal<DateFormat> threadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));
    public static void main(String[] args) throws InterruptedException {
        final Semaphore semaphore = new Semaphore(THREAD_COUNT);
        final CountDownLatch countDownLatch = new CountDownLatch(EXECUTE_COUNT);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < EXECUTE_COUNT; i++){
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    try {
                        threadLocal.get().parse("2020-01-01");
                    } catch (ParseException e) {
                        log.error("线程：" + Thread.currentThread().getName() + " 格式化日期失败");
                        log.error("异常信息：",e);
                        System.exit(1);
                    }catch (NumberFormatException e){
                        log.error("线程：" + Thread.currentThread().getName() + " 格式化日期失 败");
                        log.error("异常信息：",e);
                        System.exit(1);
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
                    log.error("信号量发生错误");
                    log.error("异常信息：",e);
                    System.exit(1);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("所有线程格式化日期成功");
    }
}
