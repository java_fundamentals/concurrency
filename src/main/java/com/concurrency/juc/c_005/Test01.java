package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test01 {
    private static final Logger log = LoggerFactory.getLogger(Test01.class);

    public static void main(String[] args) {
        // 线程1等待线程2的下载结果
        GuardedObject go = new GuardedObject();
        new Thread(() -> {
            log.info("下载中...");
            Object result = go.get();
            log.info("下载完成: {}", result);
        }, "t1").start();

        // 线程2设置下载结果
        new Thread(() -> {
            log.info("准备下载...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            go.complete("result");
            log.info("下载完成");
        }, "t2").start();

        // 线程1获取下载结果
        log.info("结果: {}",go.get());
    }
}

class GuardedObject {
    private Object response;

    // 获取结果
    public Object get() {
       synchronized (this) {
           //没有结果，则阻塞等待
           while (response == null) {
               try {
                   this.wait();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
           return response;
       }
    }

    // 设置结果
    public void complete(Object response) {
        synchronized (this) {
            this.response = response;
            this.notifyAll();
        }
    }
}