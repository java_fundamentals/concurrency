package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * park: 阻塞当前线程，直到被unpark
 * unpark: 唤醒一个被park的线程
 */
public class TestParkUnpark {
    private static final Logger log = LoggerFactory.getLogger(TestParkUnpark.class);

    public static void main(String[] args) {
       Thread t1 = new Thread(() -> {
            log.info("Thread t1 start");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("Thread t1 parking");
            LockSupport.park();
            log.info("Thread t1 resume...");
        }, "t1");
        t1.start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("Thread t1 unparking");
        LockSupport.unpark(t1);
        log.info("Thread t1 done");
    }
}
