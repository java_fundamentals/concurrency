package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

public class TestCorrectPostureStep2 {
    private static final Logger log = LoggerFactory.getLogger(TestCorrectPostureStep2.class);
    static final Object lock = new Object();
    static boolean hasCigarette = false; //有没有烟

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock) {
                log.info("有烟没？[{}]",hasCigarette);
                if (!hasCigarette) {
                    log.info("没烟，先歇会儿！");
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("歇会儿了，有烟没？[{}]",hasCigarette);
                    if (hasCigarette) {
                        log.info("可以开始干活了！");
                    }
                }
            }
        },"小南").start();

        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                synchronized (lock) {
                    log.info("可以开始干活了！");
                }
            },"其他人").start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        new Thread(()->{
            synchronized (lock) {
                hasCigarette = true;
                log.info("烟到了噢！");
                lock.notify();
            }
        },"送烟的").start();
    }
}
