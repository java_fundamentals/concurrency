package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test02 {
    private static final Logger log = LoggerFactory.getLogger(Test02.class);

    public static void main(String[] args) {
        // 线程1等待线程2的下载结果
        GuardedObject1 go = new GuardedObject1();
        new Thread(() -> {
            log.info("下载中...");
            Object result = go.get(3000);
            log.info("下载完成: {}", result);
        }, "t1").start();

        // 线程2设置下载结果
        new Thread(() -> {
            log.info("准备下载...");
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            go.complete("result");
            log.info("下载完成");
        }, "t2").start();

        // 线程1获取下载结果
        log.info("结果: {}",go.get(3000));
    }
}

class GuardedObject1 {
    private static final Logger log = LoggerFactory.getLogger(GuardedObject1.class);
    private Object response;

    /**
     * 获取结果
     * @param timeout 超时时间，单位：毫秒
     * @return 结果
     */
    public Object get(long timeout) {
       synchronized (this) {
           // 开始时间
           long start = System.currentTimeMillis();
           // 经历的时间
           long passedTime = 0;
           //没有结果，则阻塞等待
           while (response == null) {
               // 这一轮循环应该等待的时间
               long waitTime = timeout - passedTime;
               // 经历的时间超过了最大等待时间时，则退出循环
               if (waitTime <= 0) {
                   log.error("超时");
                   break;
               }
               try {
                   this.wait(waitTime); // 避免虚假唤醒
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               passedTime = System.currentTimeMillis() - start;
           }
           return response;
       }
    }

    // 设置结果
    public void complete(Object response) {
        synchronized (this) {
            this.response = response;
            this.notifyAll();
        }
    }
}