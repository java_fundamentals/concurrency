package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

public class TestCorrectPostureStep1 {
    private static final Logger log = LoggerFactory.getLogger(TestCorrectPostureStep1.class);
    static final Object lock = new Object();
    static boolean hasCigarette = false; //有没有烟

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock) {
                log.info("有烟没？[{}]",hasCigarette);
                if (!hasCigarette) {
                    log.info("没烟，先歇会儿！");
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("歇会儿了，有烟没？[{}]",hasCigarette);
                    if (hasCigarette) {
                        log.info("可以开始干活了！");
                    }
                }
            }
        },"小南").start();

        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                synchronized (lock) {
                    log.info("可以开始干活了！");
                }
            },"其他人").start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        new Thread(()->{
            /**
             * 这里能不能加 synchronized (lock)？为什么？
             * 因为这里的锁是lock，而lock是static的，所以所有线程都要竞争这个锁，
             * 所以这里不能加锁，否则会导致死锁。
             * 正确的做法是，每个线程都要获取自己的锁，而不是用一个共享的锁。
              */
//            synchronized (lock) {
                hasCigarette = true;
                log.info("烟到了噢！");
//            }
        },"送烟的").start();
    }
}
