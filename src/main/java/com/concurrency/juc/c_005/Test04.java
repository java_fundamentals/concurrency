package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * 生产者消费者模式
 * 生产者线程向消息队列中存入消息，消费者线程从消息队列中取出消息并处理
 */
public class Test04 {
    private static final Logger log = LoggerFactory.getLogger(Test04.class);

    public static void main(String[] args) {
        MessageQueue queue = new MessageQueue(2);

        // 生产者线程
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            new Thread(() -> {
                queue.put(new Message(finalI, "信息"+finalI));
            },"生产者"+i).start();
        }

        // 消费者线程
        new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                Message m = queue.take();
            }
        },"消费者").start();
    }
}

/**
 * 消息队列类，java线程之间通信
 */
class MessageQueue{
    private static final Logger log = LoggerFactory.getLogger(MessageQueue.class);
    /**
     * 消息队列集合
     */
    private final LinkedList<Message> queue = new LinkedList<>();
    /**
     * 队列容量
     */
    private final int capcity;

    public MessageQueue(int capcity){
        this.capcity = capcity;
    }

    /**
     * 获取消息
     */
    public Message take() {
        // 先判断队列是否为空
        // 若为空，则阻塞等待
        // 若不为空，则取出队首消息并返回
        synchronized (queue) {
            while (queue.isEmpty()) {
                try {
                    log.info("队列为空，消费者线程等待");
                    queue.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            // 取出队首消息并返回
            Message m = queue.removeFirst();
            log.info("消费者{}消费了消息{}", m.getId(), m.getValue());
            // 通知等待的线程
            queue.notifyAll();
            return m;
        }
    }

    /**
     * 存消息
     */
    public void put (Message m){
        synchronized (queue) {
            // 若队列已满，则阻塞等待
            // 若队列未满，则存入消息
            while (queue.size() == capcity) {
                try {
                    log.info("队列已满，生产者线程等待");
                    queue.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            // 存入消息
            queue.addLast(m);
            log.info("生产者{}生产了消息{}", m.getId(), m.getValue());
            // 通知等待的线程
            queue.notifyAll();
        }
    }
}

final class Message {
    private final int id;
    private final Object value;

    public Message(int id, Object value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public Object getValue() {
        return value;
    }
}