package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 虚假唤醒问题
 * 解决方案：
 * 1. 使用notifyAll()方法通知所有等待线程
 */
public class TestCorrectPostureStep3 {
    private static final Logger log = LoggerFactory.getLogger(TestCorrectPostureStep3.class);
    static final Object lock = new Object();
    static boolean hasCigarette = false; //有没有烟
    static boolean hasTakeout = false; //有没有外卖

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock) {
                log.info("有烟没？[{}]",hasCigarette);
                if (!hasCigarette) {
                    log.info("没烟，先歇会儿！");
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("歇会儿了，有烟没？[{}]",hasCigarette);
                    if (hasCigarette) {
                        log.info("可以开始干活了！");
                    } else {
                        log.info("烟没到，没干成活！");
                    }
                }
            }
        },"小南").start();

        new Thread(()->{
            synchronized (lock) {
                log.info("有外卖没？[{}]",hasTakeout);
                if (!hasTakeout) {
                    log.info("没外卖，先歇会儿！");
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("歇会儿了，有外卖没？[{}]",hasTakeout);
                    if (hasTakeout) {
                        log.info("可以开始干活了！");
                    } else {
                        log.info("外卖没到，没干成活！");
                    }
                }
            }
        },"小花").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        new Thread(()->{
            synchronized (lock) {
                hasTakeout = true;
                log.info("外卖到了噢！");
                lock.notifyAll();
            }
        },"送外卖的").start();
    }
}
