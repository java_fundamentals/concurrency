package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class TestWaitNotify {
    private static final Logger log = LoggerFactory.getLogger(TestWaitNotify.class);

    static final Object lock = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (lock) {
                log.info("执行...");
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("其它代码...");
            }
        },"t1").start();

        new Thread(() -> {
            synchronized (lock) {
                log.info("执行...");
                try {
                    lock.wait(); // 这里会阻塞，因为t1线程已经执行完毕，所以t2线程只能等待
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
                log.info("其它代码...");
            }
        },"t2").start();

        //主线程两秒后执行
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("唤醒lock上其他线程...");
        synchronized (lock) {
//            lock.notify(); // 唤醒t1线程
            lock.notifyAll(); // 唤醒lock上其他线程
        }
    }
}
