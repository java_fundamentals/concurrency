package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 信箱测试
 * 1. 信件产生者产生信件，并将信件放入信箱
 * 2. 信件收件者从信箱中取出信件，并处理信件
 */
public class Test03 {
    private static final Logger log = LoggerFactory.getLogger(Test03.class);

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new People().start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Mailboxes.getIds().forEach(id -> {
            Postman p = new Postman(id, "信件内容"+id);
            p.start();
        });
    }
}

class People extends Thread {
    private static final Logger log = LoggerFactory.getLogger(People.class);

    @Override
    public void run() {
        // 收信
        GuardedObject2 go = Mailboxes.createGuardedObject();
        log.info("开始收信：{}",go.getId());
        Object mail = go.get(3000);
        log.info("收到信件 id: {}，内容: {}",go.getId(), mail);
    }
}

class Postman extends Thread {
    private static final Logger log = LoggerFactory.getLogger(Postman.class);

    private final int mailId;
    private final String mail;

    public Postman(int mailId, String mail) {
        this.mailId = mailId;
        this.mail = mail;
    }

    @Override
    public void run() {
        GuardedObject2 go = Mailboxes.getGuardedObject(mailId);
        log.info("开始寄信：{}",go.getId());
        go.complete(mail);
        log.info("信件已寄出 id: {}，内容: {}",go.getId(), mail);
    }
}

class Mailboxes {
    private static final Map<Integer, GuardedObject2> boxes = new Hashtable<>();

    private static int id = 1;

    /**
     * 产生唯一的ID
     * @return ID
     */
    public static synchronized int generateId() {
        return id++;
    }

    public static GuardedObject2 createGuardedObject() {
        GuardedObject2 go = new GuardedObject2(generateId());
        boxes.put(go.getId(), go);
        return go;
    }

    public static Set<Integer> getIds() {
        return boxes.keySet();
    }

    public static GuardedObject2 getGuardedObject(int id) {
        return boxes.remove(id);
    }
}

class GuardedObject2 {
    private static final Logger log = LoggerFactory.getLogger(GuardedObject2.class);

    private int id;

    public GuardedObject2(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    private Object response;

    /**
     * 获取结果
     * @param timeout 超时时间，单位：毫秒
     * @return 结果
     */
    public Object get(long timeout) {
       synchronized (this) {
           // 开始时间
           long start = System.currentTimeMillis();
           // 经历的时间
           long passedTime = 0;
           //没有结果，则阻塞等待
           while (response == null) {
               // 这一轮循环应该等待的时间
               long waitTime = timeout - passedTime;
               // 经历的时间超过了最大等待时间时，则退出循环
               if (waitTime <= 0) {
                   log.error("超时");
                   break;
               }
               try {
                   this.wait(waitTime); // 避免虚假唤醒
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               passedTime = System.currentTimeMillis() - start;
           }
           return response;
       }
    }

    // 设置结果
    public void complete(Object response) {
        synchronized (this) {
            this.response = response;
            this.notifyAll();
        }
    }
}