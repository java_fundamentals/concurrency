package com.concurrency.juc.c_005;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 多锁模式
 * 多个线程需要访问同一资源，但是又不能同时访问，这时就可以使用多锁模式。
 * 多锁模式是指多个线程分别持有不同的锁，互不干扰地访问同一资源。
 * 多锁模式的优点是可以提高并发度，避免资源竞争，缺点是增加了复杂度。
 */
public class TestMultiLock {
    private static final Logger log = LoggerFactory.getLogger(TestMultiLock.class);

    public static void main(String[] args) {
        BigRoom bigRoom = new BigRoom();
        new Thread(bigRoom::sleep, "t1").start();

        new Thread(bigRoom::study, "t2").start();
    }
}

class BigRoom {
    private static final Logger log = LoggerFactory.getLogger(BigRoom.class);

    private final Object studyRoom = new Object();
    private final Object bedRoom = new Object();

    public void sleep() {
        synchronized (bedRoom) {
            log.info("sleeping 2小时...");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void study() {
        synchronized (studyRoom) {
            log.info("study 1小时...");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}