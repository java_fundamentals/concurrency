package com.concurrency.juc.c_008;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 顺序执行：要求t1线程必须等待t2线程运行完毕才能打印1
 * 使用ReentrantLock和Condition实现
 * 注意：Condition必须在锁的保护下使用，否则会出现IllegalMonitorStateException异常
 */
public class Test03 {
    private static final Logger log = LoggerFactory.getLogger(Test03.class);
    static ReentrantLock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();
    // 标记t2线程是否运行完毕
    static boolean flag = false;

    public static void main(String[] args) {
        new Thread(() -> {
            lock.lock();
            try {
                while (!flag) {
                    condition.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            log.info("1");
        }, "t1").start();

        new Thread(() -> {
            lock.lock();
            try {
                log.info("2");
                flag = true;
                // 通知t1线程
                condition.signal();
            } finally {
                lock.unlock();
            }
        }, "t2").start();
    }
}
