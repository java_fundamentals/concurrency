package com.concurrency.juc.c_008;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.locks.LockSupport;

/**
 * 顺序执行：要求t1线程必须等待t2线程运行完毕才能打印1
 * 方案：t1线程启动后，调用LockSupport.park()方法，使当前线程暂停，直到被unpark()唤醒。
 * t2线程启动后，调用LockSupport.unpark(t1)方法，唤醒t1线程，使其继续执行。
 * 结果：t2线程打印2后，t1线程再打印1
 * 说明：t1线程调用park()方法后，会释放当前线程的许可，使得其他线程有机会执行。
 * 因此，t1线程调用unpark(t1)方法后，会将t1线程的许可重新获取，使得t1线程继续执行。
 */
public class Test02 {
    private static final Logger log = LoggerFactory.getLogger(Test02.class);

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            LockSupport.park();
            log.info("1");
        },"t1");

        t1.start();

        new Thread(() -> {
            log.info("2");
            LockSupport.unpark(t1);
        },"t2").start();
    }
}
