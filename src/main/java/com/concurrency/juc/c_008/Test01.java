package com.concurrency.juc.c_008;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 顺序执行：要求t1线程必须等待t2线程运行完毕才能打印1
 * 解决方案：使用Object.wait()和Object.notify()方法实现线程间的同步
 * 1. 首先，t1线程启动后，会先判断t2是否已经运行完毕，如果t2没有运行完毕，则会进入同步代码块，调用Object.wait()方法，
 *    让出CPU执行权限，并进入阻塞状态，直到t2线程运行完毕，t1线程才能继续执行。
 * 2. 当t2线程运行完毕后，会调用Object.notify()方法，通知t1线程，让它继续执行。
 * 3. 由于t1线程处于阻塞状态，直到t2线程运行完毕，所以t1线程才能继续执行，并打印1。
 */
public class Test01 {
    private static final Logger log = LoggerFactory.getLogger(Test01.class);
    static final Object lock = new Object();
    // 表示t2是否已经运行
    static boolean t2Runned = false;

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            while(!t2Runned){
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            log.info("1");
        }, "t1");

        Thread t2 = new Thread(()->{
            synchronized (lock) {
                log.info("2");
                t2Runned = true;
                lock.notify();
            }
        }, "t2");

        t1.start();
        t2.start();
    }
}
