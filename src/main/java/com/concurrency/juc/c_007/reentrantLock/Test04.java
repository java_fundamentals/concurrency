package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock的tryLock(long timeout, TimeUnit unit)方法
 * 尝试获取锁，最多等待timeout时间，如果在timeout时间内获得锁，则返回true，否则返回false
 * 如果在timeout时间内线程被中断，则抛出InterruptedException
 * 如果锁已经被其他线程获取，则当前线程将进入等待状态，直到锁被释放
 */
public class Test04 {
    private static final Logger log = LoggerFactory.getLogger(Test04.class);

    private static final ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.info("尝试获得锁");
            try {
                if (!lock.tryLock(2, TimeUnit.SECONDS)) {
                    log.info("获取不到锁");
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                log.info("获取不到锁");
                return;
            }
            try {
                log.info("获得锁");
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                log.info("释放锁成功");
            }
        }, "t1");

        lock.lock();
        log.info("获得到锁");
        t1.start();
        try {
            TimeUnit.SECONDS.sleep(1);
            log.info("释放锁");
            lock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
