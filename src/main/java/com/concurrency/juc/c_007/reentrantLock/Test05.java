package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Test05 {
    private static final Logger log = LoggerFactory.getLogger(Test05.class);

    public static void main(String[] args) {
        Chopstick c1 = new Chopstick("1");
        Chopstick c2 = new Chopstick("2");
        Chopstick c3 = new Chopstick("3");
        Chopstick c4 = new Chopstick("4");
        Chopstick c5 = new Chopstick("5");
        Thread t1 = new Philosopher("老子", c1, c2);
        Thread t2 = new Philosopher("墨子", c2, c3);
        Thread t3 = new Philosopher("孔子", c3, c4);
        Thread t4 = new Philosopher("庄子", c4, c5);
        Thread t5 = new Philosopher("韩非子", c5, c1);
        t1.setName("老子");
        t2.setName("墨子");
        t3.setName("孔子");
        t4.setName("庄子");
        t5.setName("韩非子");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}

/**
 * 死锁的例子
 * 五个哲学家围着一张圆桌，每个哲学家手持一根筷子，思考问题。
 * 哲学家们的思考方式是互相抢夺对方的筷子，如果两个哲学家同时抢夺对方的筷子，就会发生死锁。
 * 为了避免死锁，哲学家们必须按照一定的顺序思考问题，并且每次只思考一个问题。
 * 为了避免死锁，我们可以让每个哲学家都按照编号的顺序思考问题，这样就不会发生死锁。
 */
class Philosopher extends Thread {
    private static final Logger log = LoggerFactory.getLogger(Philosopher.class);
    final String name;
    final Chopstick left;
    final Chopstick right;

    public Philosopher(String name, Chopstick left, Chopstick right) {
        this.name = name;
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true) {
            //尝试获得左手筷子
            if(left.tryLock()) {
                try {
                    //尝试获得右手筷子
                    if(right.tryLock()) {
                        try {
                            eat();
                        } finally {
                            right.unlock();
                        }
                    }
                } finally {
                    //释放左手筷子
                    left.unlock();
                }
            }
        }
    }

    public void eat() {
        log.info(Thread.currentThread().getName()+"吃饭了");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Chopstick extends ReentrantLock {
    private final String name;

    public Chopstick(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "筷子{"+name+"}";
    }
}
