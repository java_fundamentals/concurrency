package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock的tryLock()方法
 * 尝试获取锁，如果获取成功，则返回true，否则返回false。
 * 如果锁已经被其他线程获取，则当前线程不会等待，直接返回false。
 * 该方法不会阻塞线程，如果尝试获取锁失败，则不会等待，直接返回false。
 * 该方法适用于那些不要求锁必须被获取的场景，比如尝试获取锁，如果失败则继续执行，而不是一直等待。
 */
public class Test03 {
    private static final Logger log = LoggerFactory.getLogger(Test03.class);

    private static final ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.info("尝试获得锁");
            if (!lock.tryLock()) {
                log.info("获取不到锁");
                return;
            }
            try {
                log.info("获得锁");
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                log.info("释放锁");
            }
        }, "t1");

        lock.lock();
        log.info("获得到锁");
        t1.start();
    }
}
