package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock的Condition示例
 */
public class Test06 {
    private static final Logger log = LoggerFactory.getLogger(Test06.class);
    static ReentrantLock lock = new ReentrantLock();
    static boolean hasCigarette = false; //有没有烟
    static boolean hasTakeout = false; //有没有外卖
    // 等烟条件
    static Condition cigaretteCondition = lock.newCondition();
    // 等外卖条件
    static Condition takeoutCondition = lock.newCondition();

    public static void main(String[] args) {
        new Thread(()->{
            lock.lock();
            try {
                log.info("有烟没？[{}]",hasCigarette);
                while (!hasCigarette) {
                    log.info("没烟，先歇会儿！");
                    try {
                        cigaretteCondition.await();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("可以开始干活了！");
                }
            } finally {
                lock.unlock();
            }
        },"小南").start();

        new Thread(()->{
            lock.lock();
            try {
                log.info("有外卖没？[{}]",hasTakeout);
                while (!hasTakeout) {
                    log.info("没外卖，先歇会儿！");
                    try {
                        takeoutCondition.await();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("可以开始干活了！");
                }
            } finally {
                lock.unlock();
            }
        },"小花").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        new Thread(()->{
            lock.lock();
            try {
                hasTakeout = true;
                log.info("外卖到了噢！");
                takeoutCondition.signal();
            } finally {
                lock.unlock();
            }
        },"送外卖的").start();

        new Thread(()->{
            lock.lock();
            try {
                hasCigarette = true;
                log.info("烟到了噢！");
                cigaretteCondition.signal();
            } finally {
                lock.unlock();
            }
        },"送烟的").start();
    }
}
