package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock的中断
 * 调用lockInterruptibly()方法可以中断线程的阻塞状态，使线程退出阻塞状态并抛出InterruptedException异常。
 * 注意：调用interrupt()方法只是将线程的中断状态设置为true，并不会中断线程的阻塞状态。
 * 因此，如果线程处于阻塞状态，调用interrupt()方法并不会中断线程的运行。
 */
public class Test02 {
    private static final Logger log = LoggerFactory.getLogger(Test02.class);

    private static final ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            try {
                // 如果没有竞争那么此方法就会获取lock对象锁
                // 若果有竞争就会进入阻塞队列，可以被其他线程用interrupt方法中断
                log.info("尝试获取锁");
                lock.lockInterruptibly();
            } catch (InterruptedException e) {
                e.printStackTrace();
                log.error("没有获得锁，返回");
                return;
            }
            try {
                log.info("获取到锁");
            } finally {
                lock.unlock();
            }
        },"t1");

        lock.lock();
        t1.start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("中断t1线程");
        t1.interrupt();
    }
}
