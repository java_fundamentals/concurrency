package com.concurrency.juc.c_007.reentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock的可重入性
 */
public class Test01 {
    private static final Logger log = LoggerFactory.getLogger(Test01.class);

    private static final ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        lock.lock();
        try {
            log.info("Lock acquired");
            TimeUnit.SECONDS.sleep(1);
            m1();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            log.info("Lock released");
        }
    }

    public static void m1() {
        lock.lock();
        try {
            log.info("Lock acquired in m1");
            TimeUnit.SECONDS.sleep(1);
            m2();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            log.info("Lock released in m1");
        }
    }

    public static void m2() {
        lock.lock();
        try {
            log.info("Lock acquired in m2");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            log.info("Lock released in m2");
        }
    }
}
