package com.concurrency.juc.c_009;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 交替运行三个线程，输出abcabcabcabc...
 * 使用WaitNofify类来实现线程间的通信。
 * 线程a、b、c通过wait()和notify()方法来实现线程间的通信。
 * 线程a、b、c通过flag变量来标识自己正在运行，并通过waitFlag和nextFlag变量来控制线程的运行顺序。
 * 线程a、b、c通过打印字符来实现交替运行。
 * 输出结果为：
 * a
 * b
 * c
 * a
 * b
 * c
 * a
 * b
 * c
 *...
 */
public class Test01 {
    private static final Logger log = LoggerFactory.getLogger(Test01.class);
    public static void main(String[] args) {
        WaitNofify waitNofify = new WaitNofify(0, 10);
        new Thread(() -> waitNofify.print("a", 0, 1),"t1").start();
        new Thread(() -> waitNofify.print("b", 1, 2),"t2").start();
        new Thread(() -> waitNofify.print("c", 2, 0),"t3").start();
    }
}

class WaitNofify {
    private static final Logger log = LoggerFactory.getLogger(WaitNofify.class);
    /**
     * 打印字符，并通知其他线程继续运行。
     * @param str 要打印的字符
     * @param waitFlag 等待的线程标识
     * @param nextFlag 通知的线程标识
     */
    public void print(String str,int waitFlag,int nextFlag){
        for (int i = 0; i < loopNumber; i++) {
            synchronized (this) {
                while (flag!= waitFlag) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                log.info(str);
                flag = nextFlag;
                this.notifyAll();
            }
        }
    }

    /**
     * flag 用来标识线程的状态，0表示线程a正在运行，1表示线程b正在运行，2表示线程c正在运行。
     * 初始值为0，表示所有线程都处于等待状态。
     * 线程a、b、c通过wait()和notify()方法来实现线程间的通信。
     *
     */
    private int flag;
    /**
     * 循环次数，用来控制输出的次数。
     */
    private final int loopNumber;

    public WaitNofify(int flag, int loopNumber) {
        this.flag = flag;
        this.loopNumber = loopNumber;
    }
}
