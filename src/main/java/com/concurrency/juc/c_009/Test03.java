package com.concurrency.juc.c_009;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.locks.LockSupport;

/**
 * 交替运行三个线程，输出abcabcabcabc...
 * 实现方式：
 * 1. 定义一个类ParkUnpark，里面有一个print方法，参数为字符串和下一个线程。
 * 2. 定义三个线程t1、t2、t3，并设置t1的run方法为调用ParkUnpark的print方法，参数为"a"和t2，t2的run方法为调用ParkUnpark的print方法，参数为"b"和t3，t3的run方法为调用ParkUnpark的print方法，参数为"c"和t1。
 * 3. 启动三个线程。
 * 4. 启动t1线程，并调用LockSupport.unpark(t1)方法，使其运行。
 * 5. 三个线程交替运行，输出abcabcabcabc...。
 */
public class Test03 {
    static Thread t1;
    static Thread t2;
    static Thread t3;

    public static void main(String[] args) {
        ParkUnpark p = new ParkUnpark(10);
        t1 = new Thread(() -> p.print("a",t2), "t1");
        t2 = new Thread(() -> p.print("b",t3), "t2");
        t3 = new Thread(() -> p.print("c",t1), "t3");
        t1.start();
        t2.start();
        t3.start();
        LockSupport.unpark(t1);
    }
}

class ParkUnpark {
    private static final Logger log = LoggerFactory.getLogger(ParkUnpark.class);
    /**
     * 循环次数
     */
    private final int loopNumber;
    public ParkUnpark(int loopNumber) {
        this.loopNumber = loopNumber;
    }

    public void print(String str,Thread next){
        for (int i = 0; i < loopNumber; i++) {
            LockSupport.park();
            log.info(str);
            LockSupport.unpark(next);
        }
    }
}