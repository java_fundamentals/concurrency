package com.concurrency.juc.c_009;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 交替运行三个线程，输出abcabcabcabc...
 * 使用ReentrantLock和Condition实现
 * 线程a等待b线程通知，线程b等待c线程通知，线程c等待a线程通知
 * 线程a通知后，b线程通知，c线程通知，依次类推
 * 线程a、b、c的打印顺序为abcabcabcabc...
 */
public class Test02 {
    public static void main(String[] args) {
        AwaitSignal lock = new AwaitSignal(10);
        Condition a = lock.newCondition();
        Condition b = lock.newCondition();
        Condition c = lock.newCondition();
        new Thread(() -> lock.print("a", a, b),"t1").start();
        new Thread(() -> lock.print("b", b, c),"t2").start();
        new Thread(() -> lock.print("c", c, a),"t3").start();
        // 一秒后唤醒a条件对应的线程
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        lock.lock();
        try {
            a.signalAll();
        } finally {
            lock.unlock();
        }
    }
}

class AwaitSignal extends ReentrantLock {
    private static final Logger log = LoggerFactory.getLogger(AwaitSignal.class);
    /**
     * 控制循环次数
     */
    private final int loopNumber;

    public AwaitSignal(int loopNumber) {
        this.loopNumber = loopNumber;
    }

    /**
     * 打印指定字符，并通知下一个线程
     * @param str 打印的字符
     * @param currentCondition 当前线程的条件
     * @param nextCondition 下一个线程的条件
     */
    public void print(String str,Condition currentCondition,Condition nextCondition){
        for (int i = 0; i < loopNumber; i++) {
            lock();
            try {
                currentCondition.await();
                log.info(str);
                nextCondition.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                unlock();
            }
        }
    }
}