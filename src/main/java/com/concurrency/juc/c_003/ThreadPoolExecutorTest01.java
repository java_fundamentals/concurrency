package com.concurrency.juc.c_003;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * LinkedBlockingQueue 是一个基于链表实现的无限队列。当你将一个 LinkedBlockingQueue 实例作为线程池的队列使用时，
 * 如果这个队列没有指定容量（或者指定了一个很大的容量），那么线程池的 maximumPoolSize 可能不会起作用，
 * 因为队列可以无限扩展以容纳更多的任务，从而通常不会有足够的任务积压去触发创建超过 corePoolSize 的线程。
 * 因此，为了防止线程池的资源耗尽，建议使用有界队列（如 ArrayBlockingQueue）来代替 LinkedBlockingQueue。
 * 以下是如何验证 maximumPoolSize 不起作用的方法：
 *
 * 在这个示例中，我们提交了100个任务，而核心线程数为5。如果 maximumPoolSize 起作用，那么线程池中的线程数应该会增加到10。
 * 但是，由于队列是无限的，线程数应该不会超过 corePoolSize，即5。
 *
 * 在运行此代码时，你应该会看到输出显示线程池的大小保持在5，这证明了在这种情况下 maximumPoolSize 并没有起作用。
 * 注意，这个验证是在任务执行时间较长，并且任务数量远大于核心线程数时进行的。如果任务执行得非常快，那么可能会短暂地看到线程数超过核心线程数，
 * 但这只是因为线程池在尝试满足高并发的任务提交。一旦任务提交速度减慢，线程池的大小就会回到核心线程数。
 * @author liuc
 * @date 2024-10-14 16:27
 */
public class ThreadPoolExecutorTest01 {
    private static final Logger log = LoggerFactory.getLogger(ThreadPoolExecutorTest01.class);

    public static void main(String[] args) throws InterruptedException {
        // 核心线程数
        int corePoolSize = 5;
        // 最大线程数（理论上不会达到这个数，因为队列是无限的）
        int maximumPoolSize = 10;
        // 保持存活的时间，当线程数大于核心线程数时，这是多余空闲线程在终止前等待新任务的最长时间
        long keepAliveTime = 1;
        // 时间单位
        TimeUnit timeUnit = TimeUnit.SECONDS;
        // 任务队列，不设置容量，即为无限队列
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();
        // 创建线程池
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                timeUnit,
                workQueue
        );
        // 提交大量任务
        for (int i = 0; i < 100; i++) {
            executor.submit(() -> {
                try {
                    // 模拟任务执行时间
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                System.out.println(Thread.currentThread().getName() + " is running");
            });
        }
        // 每隔一段时间打印线程池的信息
        while (true) {
            int poolSize = executor.getPoolSize();
            System.out.println("Current pool size: " + poolSize);
            if (poolSize > corePoolSize) {
                System.out.println("maximumPoolSize is in effect!");
            } else {
                System.out.println("maximumPoolSize is NOT in effect!");
            }
            Thread.sleep(5000);
        }
    }
}
