package com.concurrency.juc.c_000;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.*;

/**
 * 怎么创建线程？
 * 1. 继承Thread类
 * 2. 实现Runnable接口
 * 3. 使用Lambda表达式
 * 4. 使用线程池
 * 5. 使用ExecutorService
 * 6. 使用线程工厂
 * 7. 使用Callable接口
 */
public class T02_HowToCreateThread {
    private static final Logger log = LoggerFactory.getLogger(T02_HowToCreateThread.class);

    /**
     * 1. 继承Thread类
     */
    static class MyThread extends Thread {
        @Override
        public void run() {
            log.info("MyThread is running");
        }
    }

    /**
     * 2. 实现Runnable接口
     */
    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            log.info("MyRunnable is running");
        }
    }

    static class RunnableTask implements Runnable {
        @Override
        public void run() {
            // 在这里执行任务逻辑
            log.info("使用ExecutorService提交的RunnableTask is running");
        }
    }

    static class MyCallableTest implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            log.info("创建线程:" + Thread.currentThread().getName());
            return 2;
        }
    }

    public static void main(String[] args) {
        /**
         * 1. 继承Thread类
         */
        MyThread myThread = new MyThread();
        myThread.start();

        /**
         * 2. 实现Runnable接口
         */
        MyRunnable myRunnable = new MyRunnable();
        Thread thread = new Thread(myRunnable);
        thread.start();
        /**
         * 3. 使用Lambda表达式
         */
        new Thread(() -> log.info("Lambda Runnable is running")).start();
        /**
         * 4. 使用线程池
         */
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.submit(new Runnable() {
            @Override
            public void run() {
                //执行业务逻辑
                for(int i = 1; i <= 10; i++) {
                    log.info("线程:" + Thread.currentThread().getName() + "执行了任务" + i + "~");
                }
            }
        });
        pool.submit(new Runnable() {
            @Override
            public void run() {
                //执行业务逻辑
                for(int i = 11; i <= 20; i++) {
                    log.info("线程:" + Thread.currentThread().getName() + "执行了任务" + i + "~");
                }
            }
        });
        pool.submit(new Runnable() {
            @Override
            public void run() {
                //执行业务逻辑
                for(int i = 21; i <= 30; i++) {
                    log.info("线程:" + Thread.currentThread().getName() + "执行了任务" + i + "~");
                }
            }
        });
        // 关闭线程池
        pool.shutdown();

        /**
         * 5. 使用ExecutorService
         */
        // 创建一个固定大小的线程池
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        // 提交任务到线程池执行
        Future<?> future = executorService.submit(new RunnableTask());

        // 关闭线程池，以优雅的方式
        executorService.shutdown();

        try {
            // 等待任务执行完毕，或者在指定的时间内等待
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * 6. 使用线程工厂
         */
        MyThreadFactory factory=new MyThreadFactory("MyThreadFactory");
        // Creates a task
        Task task=new Task();
        Thread thread1;

        // Creates and starts ten Thread objects
        log.info("Starting the Threads\n");
        for (int i=0; i<10; i++){
            thread1=factory.newThread(task);
            thread1.start();
        }
        // Prints the statistics of the ThreadFactory to the console
        log.info("Factory stats:\n");
        log.info("{}",factory.getStats());

        /**
         * 7. 使用Callable接口
         */
        FutureTask<Integer> task2 = new FutureTask<>(new MyCallableTest());
        Thread thread2 = new Thread(task2);
        thread2.start();
        try {
            log.info("创建线程的返回结果为:" + task2.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
