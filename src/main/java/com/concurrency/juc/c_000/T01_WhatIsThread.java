package com.concurrency.juc.c_000;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 什么是线程？
 * 线程是操作系统能够进行运算调度的最小单位，它被包含在进程之中，是进程中的实际运作单位。
 * 线程可以执行不同的任务，同一进程中的多个线程可以共享进程的资源。
 * 使用方式：
 * 1. 创建线程：通过继承Thread类或者实现Runnable接口创建线程。
 * 2. 启动线程：调用线程对象的start()方法启动线程。
 * 3. 等待线程结束：调用线程对象的join()方法等待线程结束。
 * 4. 停止线程：调用线程对象的interrupt()方法停止线程。
 */
public class T01_WhatIsThread {
    private static final Logger log = LoggerFactory.getLogger(T01_WhatIsThread.class);
    private static class T1 extends Thread {
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    TimeUnit.MICROSECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("T1: " + i);
            }
        }
    }

    public static void main(String[] args) {
        new T1().start();
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.MICROSECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("Main: " + i);
        }
    }
}
