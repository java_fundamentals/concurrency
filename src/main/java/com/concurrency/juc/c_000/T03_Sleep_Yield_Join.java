package com.concurrency.juc.c_000;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * sleep、yield 和 join 是在多线程编程中常用的方法，它们分别用于控制线程的执行和同步。下面是它们的区别：
 *
 * sleep: sleep 方法是用来暂停当前线程的执行一段时间。在调用 sleep 方法后，当前线程会暂停执行指定的时间，然后恢复执行。
 * sleep 方法接受一个时间参数，参数的单位通常是毫秒。在这段时间内，线程将不会参与 CPU 调度，但资源仍被保留。
 * 这意味着在调用 sleep 后，线程将会释放 CPU 时间，但是占用其他资源，例如内存等，线程处于阻塞状态。
 *
 * yield: yield 方法是用来让出 CPU 时间，让其他线程有机会执行。调用 yield 方法的线程会让出 CPU 时间，然后重新参与 CPU 调度。
 * yield 方法的效果是告诉调度器：当前线程愿意让出一部分 CPU 时间，让其他线程执行。调用 yield 方法后，当前线程的执行状态会变为就绪状态，允许其他就绪状态的线程执行。但是并不能保证其他线程就会立刻执行。
 *
 * join: join 方法用于等待指定线程执行结束。如果在一个线程中调用另一个线程的 join 方法，那么当前线程将会等待被调用的线程执行结束。
 * 直到被调用的线程执行完毕，当前线程才会继续执行。join 方法允许一个线程在另一个线程结束之前等待，通常用于线程同步。
 * 综上所述，sleep 用于暂停当前线程的执行，yield 用于让出 CPU 时间，join 用于等待其他线程执行结束。这些方法在多线程编程中具有不同的作用，开发人员可以根据具体的需求选择合适的方法来控制线程的执行。
 *
 * 注意：
 * 1. sleep方法和yield方法都不会释放对象锁，所以如果有同步代码块，则会造成线程阻塞。
 * 2. 建议使用join方法，因为它可以让主线程等待子线程执行完毕后再继续执行。
 * 3. 建议使用TimeUnit.SECONDS.sleep方法，因为它可以让线程休眠指定的时间。
 *
 */
public class T03_Sleep_Yield_Join {
    private static final Logger log = LoggerFactory.getLogger(T03_Sleep_Yield_Join.class);
    public static void main(String[] args){
//        testSleep();
//        testYield();
        testJoin();
    }

    public static void testSleep(){
        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-1: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-2: " + i);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public static void testYield(){
        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-1: " + i);
                Thread.yield();
            }
        }).start();

        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-2: " + i);
                Thread.yield();
            }
        }).start();
    }

    public static void testJoin(){
        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-1: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            for(int i=0; i<10; i++){
                log.info("Thread-2: " + i);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
