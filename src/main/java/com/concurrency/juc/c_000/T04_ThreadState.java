package com.concurrency.juc.c_000;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 多线程状态
 * 线程状态包括：NEW、RUNNABLE、BLOCKED、WAITING、TIMED_WAITING、TERMINATED
 * 线程状态的切换：
 * 1. NEW -> RUNNABLE：线程启动后，状态从NEW变为RUNNABLE
 * 2. RUNNABLE -> BLOCKED：线程被阻塞，如调用了wait()方法，状态从RUNNABLE变为BLOCKED
 * 3. RUNNABLE -> WAITING：线程调用了wait()方法，状态从RUNNABLE变为WAITING
 * 4. RUNNABLE -> TIMED_WAITING：线程调用了sleep()方法，状态从RUNNABLE变为TIMED_WAITING
 * 5. BLOCKED -> RUNNABLE：线程从BLOCKED状态恢复，状态从BLOCKED变为RUNNABLE
 * 6. WAITING -> RUNNABLE：线程从WAITING状态恢复，状态从WAITING变为RUNNABLE
 * 7. TIMED_WAITING -> RUNNABLE：线程从TIMED_WAITING状态恢复，状态从TIMED_WAITING变为RUNNABLE
 * 8. RUNNABLE -> TERMINATED：线程执行完毕，状态从RUNNABLE变为TERMINATED
 * 9. 异常终止：线程抛出异常，状态从RUNNABLE变为TERMINATED
 * 10. 调用interrupt()方法：线程调用interrupt()方法，状态从RUNNABLE变为INTERRUPTED
 * 11. 调用stop()方法：线程调用stop()方法，状态从RUNNABLE变为TERMINATED
 * 12. 程序退出：线程执行完毕，状态从RUNNABLE变为TERMINATED
 * 13. 调用Thread.yield()方法：线程调用yield()方法，状态从RUNNABLE变为RUNNABLE
 * 14. 调用Object.wait()方法：线程调用Object.wait()方法，状态从RUNNABLE变为WAITING
 * 15. 调用Thread.join()方法：线程调用join()方法，状态从RUNNABLE变为TERMINATED
 * 16. 调用LockSupport.park()方法：线程调用LockSupport.park()方法，状态从RUNNABLE变为BLOCKED
 * 17. 调用LockSupport.parkNanos()方法：线程调用LockSupport.parkNanos()方法，状态从RUNNABLE变为BLOCKED
 * 18. 调用LockSupport.parkUntil()方法：线程调用LockSupport.parkUntil()方法，状态从RUNNABLE变为TIMED_WAITING
 * 19. 调用Object.notify()方法：线程调用Object.notify()方法，状态从WAITING变为RUNNABLE
 * 20. 调用Object.notifyAll()方法：线程调用Object.notifyAll()方法，状态从WAITING变为RUNNABLE
 * 21. 调用LockSupport.unpark()方法：线程调用LockSupport.unpark()方法，状态从BLOCKED或TIMED_WAITING变为RUNNABLE
 * 22. 调用Thread.setDaemon()方法：线程调用setDaemon()方法，状态从RUNNABLE变为RUNNABLE或TERMINATED
 * 23. 调用Thread.setName()方法：线程调用setName()方法，状态从RUNNABLE变为RUNNABLE
 * 24. 调用Thread.setPriority()方法：线程调用setPriority()方法，状态从RUNNABLE变为RUNNABLE
 * 25. 调用Thread.sleep()方法：线程调用sleep()方法，状态从RUNNABLE变为TIMED_WAITING
 */
public class T04_ThreadState {
    private static final Logger log = LoggerFactory.getLogger(T04_ThreadState.class);

    public static void main(String[] args) {
        MyThread thread = new MyThread();
        log.info("Thread state: " + thread.getState());
        thread.start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("Thread state: " + thread.getState());
        thread.interrupt();
        log.info("Thread state: " + thread.getState());
    }

    static class MyThread extends Thread {
        @Override
        public void run() {
            log.info("Thread state: " + this.getState());
            for (int i = 0; i < 10; i++) {
                try {
                    log.info(""+i);
                    TimeUnit.MICROSECONDS.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
