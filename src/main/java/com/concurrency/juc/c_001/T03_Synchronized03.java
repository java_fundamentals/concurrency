package com.concurrency.juc.c_001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class T03_Synchronized03 {
    private static final Logger log = LoggerFactory.getLogger(T03_Synchronized03.class);
    private static int count = 0;

    /**
     * 这里的synchronized关键字是用来修饰方法的，而不是修饰代码块。
     * 修饰方法的意思是，在同一时间只能有一个线程执行该方法，其他线程必须等待。
     * 修饰代码块的意思是，在同一时间只能有一个线程执行该代码块，其他线程必须等待。
     * 这里的synchronized关键字修饰的是方法，所以同一时间只能有一个线程执行该方法。
     * 而count++和count--都是原子操作，所以不需要加锁。
     * 所以，这里的synchronized关键字修饰的是方法，而不是代码块。
     * 这也是为什么在方法上使用synchronized关键字，而不是代码块的原因。
     * 另外，在方法上使用synchronized关键字，可以保证线程安全，因为同一时间只能有一个线程执行该方法。
     * 而在代码块上使用synchronized关键字，则可以保证线程安全，因为同一时间只能有一个线程执行该代码块。
     * 但是，在代码块上使用synchronized关键字，会降低程序的并发性。
     * 因为同一时间只能有一个线程执行该代码块，其他线程必须等待，这就意味着，其他线程只能等待当前线程执行完该代码块后才能执行。
     * 这就意味着，如果有多个线程同时执行该代码块，则会导致多个线程同时执行该代码块，这就降低了程序的并发性。
     * 所以，在代码块上使用synchronized关键字，应该慎用。
     * 而在方法上使用synchronized关键字，则可以保证线程安全，并且可以提高程序的并发性。
     * 所以，在方法上使用synchronized关键字，是最安全、最可靠的选择。
     *
     * 这里等同于synchronized(this) { count++; }
     */
    public static synchronized void increase() {
        count++;
        log.info(Thread.currentThread().getName() + " increase count: {}", count);
    }

    /**
     * 考虑一下这里写synchronized(this)是否可以？为什么？
     * 不可以。因为这里的synchronized关键字修饰的是方法，而不是代码块。
     * 所以，这里的synchronized(this)是没有意义的。
     * 正确的做法应该是synchronized(T03_Synchronized03.class) { count--; }。
     */
    public static void decrease() {
        synchronized (T03_Synchronized03.class) {
            count--;
            log.info(Thread.currentThread().getName() + " decrease count: {}", count);
        }
    }

    public void mm() {
        synchronized (this) {
            count++;
            log.info(Thread.currentThread().getName() + " mm count: {}", count);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    increase();
                }
            }, "thread-" + i).start();
        }

        T03_Synchronized03 t = new T03_Synchronized03();
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    t.mm();
                }
            }, "thread-" + i).start();
        }
    }
}
