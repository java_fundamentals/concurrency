package com.concurrency.juc.c_001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * synchronized关键字
 * 对某个对象加锁
 */
public class T02_Synchronized02 extends Thread {
    private static final Logger log = LoggerFactory.getLogger(T02_Synchronized02.class);

    private int count = 10;

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            myMethod();
        }
    }

    public void myMethod() {
        //任何线程访问该对象的synchronized方法，都会先获得对象的锁，然后才执行方法体
        synchronized (this) {
            count--;
            log.info(Thread.currentThread().getName() + " count = " + count);
        }
    }

    public static void main(String[] args) {
        Thread t1 = new T02_Synchronized02();
        Thread t2 = new T02_Synchronized02();
        t1.start();
        t2.start();
    }
}
