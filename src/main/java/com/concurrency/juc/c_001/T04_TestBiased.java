package com.concurrency.juc.c_001;

import org.openjdk.jol.info.ClassLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * 测试偏向锁
 * -XX:+UseBiasedLocking
 * 开启偏向锁后，JVM会对锁进行优化，偏向锁是一种特殊的锁，当一个线程获取锁时，会将锁的状态设置为偏向锁，
 * 之后该线程在进入同步代码块时，无需进行同步操作，直接执行代码块，提高性能。
 * 但是，偏向锁也有一些缺点，比如，当锁竞争激烈时，偏向锁的效率会下降，因此，在锁竞争不激烈的情况下，
 * 建议不要使用偏向锁。
 *
 * 偏向锁的状态切换过程：
 * 1. 线程第一次获取锁时，会尝试获取偏向锁，如果获取成功，则将锁的状态设置为偏向锁；
 * 2. 如果线程获取锁失败，则会继续尝试获取轻量级锁，如果获取成功，则将锁的状态设置为轻量级锁；
 * 3. 如果线程获取锁失败，则会继续尝试获取重量级锁，如果获取成功，则将锁的状态设置为重量级锁；
 * 4. 如果线程获取锁失败，则会膨胀为重量级锁，将锁的状态设置为重量级锁；
 * 5. 如果线程释放锁，则锁的状态会恢复到原来的状态。
 *
 * 偏向锁的优点：
 * 1. 加锁和解锁操作的性能随着锁的竞争而提升；
 * 2. 降低锁的获取时间，提高性能；
 * 3. 减少锁的争用，提高性能。
 *
 * 偏向锁的缺点：
 * 1. 偏向锁的线程局部性不好，容易造成线程间的资源竞争；
 * 2. 偏向锁的锁撤销操作无法做到精确，可能导致线程间的资源竞争；
 * 3. 偏向锁的锁粗化操作无法做到精确，可能导致线程间的资源竞争。
 *
 * 总结：
 * 1. 偏向锁是JVM对锁的一种优化，可以提高性能；
 * 2. 偏向锁的状态切换过程比较复杂，容易造成性能问题；
 * 3. 偏向锁的优点是降低锁的获取时间，提高性能；
 * 4. 偏向锁的缺点是线程局部性不好，容易造成资源竞争。
 * 5. 在锁竞争不激烈的情况下，建议不要使用偏向锁。
 * 6. 在锁竞争激烈的情况下，建议使用轻量级锁或重量级锁。
 * 7. JVM默认开启偏向锁，可以通过-XX:-UseBiasedLocking关闭。
 *
 * -XX:BiasedLockingStartupDelay=0 偏向锁延迟启动时间，默认延迟0秒，即JVM启动后立即启动偏向锁。
 * -XX:BiasedLockingStartupDelay=n 偏向锁延迟启动时间，n为延迟时间，单位为秒。
 *
 * 验证偏向锁对象初始化的偏向锁状态：
 * 1. 启动JVM，查看偏向锁对象初始化的偏向锁状态；
 * 2. 启动JVM，设置-XX:+UseBiasedLocking，查看偏向锁对象初始化的偏向锁状态；
 * 3. 启动JVM，设置-XX:-UseBiasedLocking，查看偏向锁对象初始化的偏向锁状态。
 */
public class T04_TestBiased {
    private static final Logger log = LoggerFactory.getLogger(T04_TestBiased.class);

    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.hashCode(); // 触发偏向锁初始化
        printBinaryAddress(dog);
        synchronized (dog) {
            printBinaryAddress(dog);
        }
        printBinaryAddress(dog);
    }

    /**
     * 打印对象的十六进制地址和二进制地址
     * @param obj 任意对象
     */
    private static void printBinaryAddress(Object obj) {
        String printable = ClassLayout.parseInstance(obj).toPrintable();
        log.info("Printable layout: \n{}", printable);
        String hexAddress = extractHexAddress(printable);
        long address = Long.parseLong(hexAddress, 16);
        String binaryAddress = Long.toBinaryString(address);
        // 确保二进制字符串是64位长，不足部分用0填充
        String paddedBinaryAddress = String.format("%64s", binaryAddress).replace(' ', '0');
        log.info("Binary address: {}", paddedBinaryAddress);
    }

    /**
     * 反射获取对象的十六进制地址
     * @param printable 任意对象
     * @return 对象的十六进制地址
     */
    private static String extractHexAddress(String printable) {
        // 正则表达式用于匹配十六进制地址
        String hexAddressPattern = "0x[0-9a-fA-F]+";
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(hexAddressPattern);
        java.util.regex.Matcher matcher = pattern.matcher(printable);

        if (matcher.find()) {
            return matcher.group(0).substring(2); // 移除前缀"0x"
        } else {
            throw new IllegalArgumentException("Hex address not found in the provided string.");
        }
    }
}

class Dog{

}
