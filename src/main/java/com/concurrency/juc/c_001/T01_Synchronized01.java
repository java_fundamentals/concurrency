package com.concurrency.juc.c_001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * synchronized是什么？
 * 1. synchronized是Java中的关键字，用来控制多线程对共享资源的访问。
 * 2. synchronized可以修饰方法、代码块或类。
 * 3. synchronized可以保证原子性，即一个synchronized方法或者代码块中的代码，要么全部执行，要么全部不执行。
 * 4. synchronized可以保证可见性，即一个线程修改了共享变量的值，其他线程能够立即看到修改后的最新值。
 * 5. synchronized可以保证互斥性，即同一时刻只有一个线程可以访问某个资源。
 * 6. synchronized可以用来创建线程安全的类。
 * 7. synchronized的缺点是效率低下，因为它使用了互斥锁，当竞争激烈时，效率会变低。
 *
 * synchronized的使用场景：
 * 1. 多个线程访问同一个对象时，需要保证线程安全。
 * 2. 多个线程访问同一个资源时，需要保证互斥访问。
 * 3. 多个线程需要共享数据，但是不需要保证原子性。
 * 4. 多个线程需要访问某个资源时，需要确保资源不会被多个线程同时访问。
 * 5. 多个线程需要访问某个资源时，需要确保资源不会被其他线程修改。
 *
 * synchronized的使用方式：
 * 1. synchronized修饰方法：
 *    synchronized修饰的方法，同一时刻只允许一个线程执行该方法，其他线程必须等待当前线程执行完毕后才能执行该方法。
 * 2. synchronized修饰代码块：
 *    synchronized修饰的代码块，同一时刻只允许一个线程执行该代码块，其他线程必须等待当前线程执行完毕后才能执行该代码块。
 * 3. synchronized修饰类：
 *    synchronized修饰的类，同一时刻只允许一个线程访问该类的所有成员方法和代码块，其他线程必须等待当前线程执行完毕后才能访问该类。
 * 4. 注意：
 *    synchronized只能锁定对象，不能锁定类。
 *    synchronized不能修饰静态方法和静态代码块。
 *    synchronized不能修饰构造方法。
 *    synchronized不能继承。
 *    synchronized不能使用在接口中。
 *    synchronized不能使用在本地方法中。
 *    synchronized不能使用在匿名类中。
 *    synchronized不能使用在数组中。
 *    synchronized不能使用在final变量上。
 *    synchronized不能使用在volatile变量上。
 *    synchronized不能使用在synchronized方法或代码块上。
 *    synchronized不能使用在线程中。
 *    synchronized不能使用在线程池中。
 *    synchronized不能使用在线程本地变量中。
 */
public class T01_Synchronized01 {
    private static final Logger log = LoggerFactory.getLogger(T01_Synchronized01.class);
    private static int count = 0;

    public static synchronized void increment() {
        count++;
    }

    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }
        }
        ).start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }
        }).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("count: {}", count);
    }
}
