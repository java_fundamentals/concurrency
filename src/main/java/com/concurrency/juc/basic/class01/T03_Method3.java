package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 创建线程的方式3: lambda表达式方式
 */
public class T03_Method3 {
    private static final Logger log = LoggerFactory.getLogger(T03_Method3.class);

    public static void main(String[] args) {
        new Thread(() -> {
            log.info("Hello from thread");
        }).start();
    }
}
