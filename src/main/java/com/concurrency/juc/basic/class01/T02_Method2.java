package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 创建线程的方式2：实现Runnable接口
 * 实现Runnable接口，并重写run()方法，在run()方法中实现线程要执行的逻辑。
 * 然后创建Thread对象，并传入Runnable对象作为构造参数，启动线程。
 */
public class T02_Method2 implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(T02_Method2.class);
    @Override
    public void run() {
        log.info("Method2 is running");
    }

    public static void main(String[] args) {
        T02_Method2 obj = new T02_Method2();
        Thread t = new Thread(obj);
        t.start();
    }
}
