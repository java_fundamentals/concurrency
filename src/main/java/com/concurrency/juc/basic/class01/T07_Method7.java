package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 创建线程的方式7：实现Callable接口
 * 实现Callable接口的类，可以返回值，并且可以抛出异常。
 * 实现Callable接口的类，可以作为参数传递给ExecutorService的submit()方法，
 */
public class T07_Method7 {
    private static final Logger log = LoggerFactory.getLogger(T07_Method7.class);

    public static void main(String[] args) {
        FutureTask<Integer> task = new FutureTask<>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.info("创建线程:" + Thread.currentThread().getName());
                return 2;
            }
        });
        Thread thread2 = new Thread(task);
        thread2.start();
        try {
            log.info("创建线程的返回结果为:" + task.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
