package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * 创建线程的方式6：实现ThreadFactory接口
 * 实现ThreadFactory接口，并重写newThread方法，在newThread方法中可以自定义线程的创建方式。
 * 实现ThreadFactory接口的好处是可以自定义线程的创建方式，比如可以设置线程的名字、线程组、线程优先级等。
 * 实现ThreadFactory接口的步骤：
 * 1. 实现ThreadFactory接口
 * 2. 重写newThread方法，在方法中创建并返回一个Thread对象
 * 3. 创建ThreadFactory对象，并调用其newThread方法创建线程
 * 4. 调用线程对象的start方法启动线程
 * 5. 调用ThreadFactory对象的getStats方法获取线程创建的统计信息
 * 6. 调用ThreadFactory对象的shutdown方法关闭线程池
 * 注意：
 *   实现ThreadFactory接口并重写newThread方法，可以创建线程，但不能控制线程的启动和关闭。
 */
public class T06_Method6 implements ThreadFactory {
    private static final Logger log= LoggerFactory.getLogger(T06_Method6.class);
    private int counter;
    private String name;
    private List<String> stats;

    public T06_Method6(String name){
        counter=0;
        this.name=name;
        stats=new ArrayList<String>();
    }

    @Override
    public Thread newThread(Runnable r) {
        // Create the new Thread object
        Thread t=new Thread(r,name+"-Thread_"+counter);
        counter++;
        // Actualize the statistics of the factory
        stats.add(String.format("Created thread %d with name %s on %s\n",t.getId(),t.getName(),new Date()));
        return t;
    }

    public String getStats(){
        StringBuffer buffer=new StringBuffer();
        Iterator<String> it=stats.iterator();

        while (it.hasNext()) {
            buffer.append(it.next());
        }

        return buffer.toString();
    }

    public static class Task implements Runnable {
        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        T06_Method6 factory=new T06_Method6("T06_Method6");
        // Creates a task
        Task task=new Task();
        Thread thread1;

        // Creates and starts ten Thread objects
        log.info("Starting the Threads\n");
        for (int i=0; i<10; i++){
            thread1=factory.newThread(task);
            thread1.start();
        }
        // Prints the statistics of the ThreadFactory to the console
        log.info("Factory stats:\n");
        log.info("{}",factory.getStats());
    }
}
