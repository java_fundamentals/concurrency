package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 创建线程的方式5：使用ExecutorService.submit()方法提交Runnable任务
 * 该方法返回一个Future对象，通过该对象可以获取任务执行结果，或者取消任务等。
 * 该方法可以方便地将Runnable任务提交到线程池执行，并获取任务执行结果。
 * 该方法的返回值Future对象可以用于判断任务是否执行完毕、取消任务、获取任务执行结果等。
 */
public class T05_Method5 {
    private static final Logger log = LoggerFactory.getLogger(T05_Method5.class);

    static class RunnableTask implements Runnable {
        @Override
        public void run() {
            // 在这里执行任务逻辑
            log.info("使用ExecutorService提交的RunnableTask is running");
        }
    }

    public static void main(String[] args) {
        // 创建一个固定大小的线程池
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        // 提交任务到线程池执行
        Future<?> future = executorService.submit(new T05_Method5.RunnableTask());
        // 关闭线程池，以优雅的方式
        executorService.shutdown();

        try {
            // 等待任务执行完毕，或者在指定的时间内等待
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
