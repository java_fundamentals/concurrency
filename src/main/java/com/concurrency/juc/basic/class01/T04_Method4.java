package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 创建线程的方式4：通过ExecutorService的execute()方法来创建线程
 * 优点：简单，易于理解
 * 缺点：无法控制线程的数量，可能会创建过多线程，造成系统资源的浪费
 * 适用场景：适用于创建少量线程的场景
 * 注意：ExecutorService的shutdown()方法必须调用，否则线程池不会立即关闭，可能造成资源泄露
 */
public class T04_Method4 {
    private static final Logger log = LoggerFactory.getLogger(T04_Method4.class);

    ExecutorService pool = Executors.newCachedThreadPool();

    public void execute(Runnable task) {
        pool.execute(task);
    }

    public void shutdown() {
        pool.shutdown();
    }

    public static void main(String[] args) {
        T04_Method4 obj = new T04_Method4();
        obj.execute(new Runnable() {
            @Override
            public void run() {
                //执行业务逻辑
                for(int i = 1; i <= 10; i++) {
                    log.info("线程:" + Thread.currentThread().getName() + "执行了任务" + i + "~");
                }
            }
        });

        obj.execute(new Runnable() {
            @Override
            public void run() {
                //执行业务逻辑
                for(int i = 11; i <= 20; i++) {
                    log.info("线程:" + Thread.currentThread().getName() + "执行了任务" + i + "~");
                }
            }
        });
        obj.shutdown();
    }
}
