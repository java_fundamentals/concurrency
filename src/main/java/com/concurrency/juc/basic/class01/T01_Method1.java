package com.concurrency.juc.basic.class01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 创建线程的方式1：继承Thread类，重写run()方法
 * 这种方式创建线程的好处是简单，只需要定义一个类继承Thread类，并重写run()方法，然后调用start()方法即可。
 */
public class T01_Method1 extends Thread {
    private static final Logger log = LoggerFactory.getLogger(T01_Method1.class);
    @Override
    public void run() {
        log.info("Running T01_Method1");
    }

    public static void main(String[] args) {
        T01_Method1 t1 = new T01_Method1();
        t1.start();
        log.info("Main thread");
    }
}
