package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 统筹调度示例
 * @author liuc
 * @date 2024-05-20 22:09
 */
public class T08_Example01 {
    private static final Logger log = LoggerFactory.getLogger(T08_Example01.class);

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.info("洗水壶！");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("烧开水！");
            try {
                TimeUnit.SECONDS.sleep(25);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        },"老王");

        Thread t2 = new Thread(() -> {
            log.info("洗茶壶！");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e){
                throw new RuntimeException(e);
            }
            log.info("洗茶杯！");
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("拿茶叶");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            try {
                t1.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("泡茶！");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        },"小李");

        t1.start();
        t2.start();
    }
}
