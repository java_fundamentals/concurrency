package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 防止线程空转占满100%的CPU资源
 */
public class T01_Sleep_Cpu extends Thread {
    private static final Logger log = LoggerFactory.getLogger(T01_Sleep_Cpu.class);

    public static void main(String[] args) {
        new Thread(() -> {
            while (true) {
                log.info("CPU is working hard...");
                try {
                    /**
                     * 这段作用是让CPU一直工作，不停的进行计算
                     * sleep(0)是干嘛的？
                     * 这段代码的作用是让线程休眠0毫秒，不要让while(true)空转，浪费CPU资源，这时可以使用Thread.yield()方法或者
                     * 让线程休眠(Thread.sleep(0))一段时间，让CPU有时间切换到其他线程，从而提高CPU的利用率。
                     */
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
