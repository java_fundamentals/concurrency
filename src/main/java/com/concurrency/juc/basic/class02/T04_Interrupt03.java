package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 两阶段终止模式
 * 1. 启动监控线程
 * 2. 停止监控线程
 * 3. 监控线程的业务逻辑
 *
 * 优点：
 * 1. 优雅的处理线程中断
 * 2. 避免了复杂的线程状态同步
 * 3. 避免了复杂的线程间通信
 * 4. 避免了死锁
 *
 * 缺点：
 * 1. 监控线程的业务逻辑需要自己实现
 * 2. 监控线程的启动和停止需要时间，影响业务逻辑的执行时间
 * 3. 监控线程的实现比较复杂
 * 4. 监控线程的实现代码容易出错
 * 5. 监控线程的实现代码不易维护
 * 6. 监控线程的实现代码不易扩展
 * 7. 监控线程的实现代码不易测试
 * 8. 监控线程的实现代码不易理解
 *
 */
public class T04_Interrupt03 {
    public static void main(String[] args) {
        TwoPhaseTermination twoPhaseTermination = new TwoPhaseTermination();
        twoPhaseTermination.start();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        twoPhaseTermination.stop();
    }
}

class TwoPhaseTermination {
    private static final Logger log = LoggerFactory.getLogger(TwoPhaseTermination.class);
    private volatile boolean terminated = false;
    // 监控线程
    private Thread monitorThread;

    // 启动监控线程
    public void start(){
        monitorThread = new Thread(() -> {
            while (!terminated) {
                // 监控线程的业务逻辑
                Thread currentThread = Thread.currentThread();
                if (currentThread.isInterrupted()) {
                    // 线程中断，退出监控线程
                    log.info("监控线程退出");
                    break;
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                    log.info("监控中...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    // 重新设置中断标志
                    currentThread.interrupt();
                    log.info("监控线程被中断，重新设置中断标志");
                    // 退出监控线程
                    break;
                }
            }
        });
        monitorThread.start();
    }

    // 停止监控线程
    public void stop() {
        terminated = true;
        // 中断监控线程
        monitorThread.interrupt();
    }
}