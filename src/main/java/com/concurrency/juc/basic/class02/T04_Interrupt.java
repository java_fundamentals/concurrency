package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用方法：interrupt()
 * 1. interrupt()方法用于中断线程的执行，使线程停止运行。
 * 2. 调用interrupt()方法后，线程会抛出InterruptedException异常，并清除中断状态。
 * 3. 调用interrupt()方法后，线程会立即停止运行，但不一定立即停止正在执行的任务。
 * 4. 调用interrupt()方法后，如果线程处于阻塞状态（如sleep、wait、join），则会抛出InterruptedException异常，并清除中断状态。
 * 5. 调用interrupt()方法后，如果线程处于运行状态，则会设置线程的中断状态为true，并通知线程。
 * 6. 调用interrupt()方法后，如果线程处于等待状态（如Object.wait()、Thread.join()），则会抛出InterruptedException异常，并清除中断状态。
 * 7. 调用interrupt()方法后，如果线程处于同步状态（如synchronized代码块），则会抛出InterruptedException异常，并清除中断状态。
 * 8. 调用interrupt()方法后，如果线程处于阻塞状态，则会抛出InterruptedException异常，并清除中断状态。
 * 9. 调用interrupt()方法后，如果线程处于死亡状态，则不会抛出InterruptedException异常。
 * 10. 调用interrupt()方法后，如果线程已经完成了任务，则不会抛出InterruptedException异常。
 *
 * 使用interrupt方法的步骤：
 * 1. 首先，判断线程是否处于运行状态，如果不是，则直接返回；
 * 2. 然后，调用interrupt()方法，设置线程的中断状态为true；
 * 3. 最后，如果线程处于阻塞状态，则会抛出InterruptedException异常，并清除中断状态。
 */
public class T04_Interrupt {
    private static final Logger log = LoggerFactory.getLogger(T04_Interrupt.class);

    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    log.info("Running");
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    log.error(Thread.currentThread().getName() + "been interrupted", e);
                    break;
                }
            }
        },"线程1");
        t.start();
        try {
            Thread.sleep(2000);
            t.interrupt();
            log.info("2 seconds passed, {} has been interrupted", t.getName());
            log.info("Thread interrupted");
        } catch (InterruptedException e) {
            log.error("Main thread interrupted");
        }
    }
}
