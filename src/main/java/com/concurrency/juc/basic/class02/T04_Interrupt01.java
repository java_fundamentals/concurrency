package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 打断正常运行的线程
 *
 * 线程的打断是通过设置线程的中断状态来实现的，可以通过Thread.interrupt()方法来设置线程的中断状态，
 * 线程可以通过捕获该异常来判断是否被打断，如果被打断，则可以进行相应的处理。
 *
 */
public class T04_Interrupt01 {
    private static final Logger log = LoggerFactory.getLogger(T04_Interrupt01.class);

    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    log.info(Thread.currentThread().getName() + " Running");
                    // 判断线程是否被打断
                    boolean interrupted = Thread.currentThread().isInterrupted();
                    if (interrupted) {
                        log.info("Thread interrupted");
                        break;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException e) {   // 捕获到InterruptedException异常，说明线程被打断
                    log.info("Thread interrupted");
                    break;
                }
            }
        }, "线程1");
        t.start();

        try {
            Thread.sleep(5000);
            t.interrupt();   // 打断线程
        } catch (InterruptedException e) {
            e.printStackTrace();    // 捕获到InterruptedException异常，说明线程被打断
        }
    }
}
