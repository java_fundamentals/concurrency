package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用方法：Thread.join()
 * 作用：让当前线程等待其他线程执行完毕后再继续执行
 * 注意：
 * 1. join()方法必须在线程启动之后调用，否则会报IllegalThreadStateException异常
 * 2. synchronized关键字和join()方法不能同时使用，否则会导致死锁现象
 * 使用场景：
 * 1. 多个线程需要等待其他线程执行完毕后再继续执行，比如多个线程需要共享某些资源，需要等待其他线程释放资源后才能继续执行
 * 2. 多个线程需要等待某个事件的发生，比如多个线程需要等待某个文件或网络连接的建立或关闭后才能继续执行
 * 3. 多个线程需要等待某个条件的满足，比如多个线程需要等待某个变量的值改变后才能继续执行
 * 4. 多个线程需要等待其他线程的退出，比如主线程需要等待子线程执行完毕后才能退出
 * 5. 多个线程需要等待其他线程的通知，比如主线程需要等待子线程通知后才能继续执行
 */
public class T03_Join {
    private static final Logger log = LoggerFactory.getLogger(T03_Join.class);

    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    log.info("Thread 1 is done");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread 2 is done");
            }
        });

        t1.start();
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        t2.start();
    }
}
