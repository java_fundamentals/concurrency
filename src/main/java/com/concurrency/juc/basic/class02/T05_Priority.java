package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用方法：优先级
 * 优先级是指线程在CPU上运行的优先级，优先级高的线程会抢占CPU资源，优先级低的线程则不会。
 * 优先级可以通过Thread.setPriority()方法设置，范围是1~10，默认优先级是5。
 * 优先级的设置对程序的运行有着重要的影响，建议在合理的范围内设置优先级，以提高程序的并发度。
 * 优先级的设置可以影响程序的响应时间，因此在一些高响应时间的场景下，优先级的设置可能是必要的。
 * 优先级的设置也会影响程序的并发度，优先级高的线程会抢占CPU资源，因此，如果有多个优先级相同的线程，则可能导致某些线程的运行时间变长。
 * 优先级的设置也会影响线程的调度，因此，如果有多个线程都处于等待状态，则可能导致线程调度的不确定性。
 * 优先级的设置对程序的稳定性也有影响，优先级高的线程可能会导致其他线程的运行变慢，因此，在一些关键的线程上设置优先级可能会导致程序的不稳定。
 * 优先级的设置可以通过Thread.getPriority()方法获取，可以通过Thread.currentThread().getPriority()方法获取当前线程的优先级。
 *
 * 注意：优先级的设置不是绝对的，它只是影响了线程调度的优先级，并不能保证一定能提高程序的并发度。
 */
public class T05_Priority {
    private static final Logger log = LoggerFactory.getLogger(T05_Priority.class);

    public static void main(String[] args) {
        Runnable r1 = () -> log.info("Low Priority");
        Runnable r2 = () -> log.info("High Priority");
        Thread t1 = new Thread(r1);
        t1.setName("线程1");
        Thread t2 = new Thread(r2);
        t2.setName("线程2");
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
    }
}
