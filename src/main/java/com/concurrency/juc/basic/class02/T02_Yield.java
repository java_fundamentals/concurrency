package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用方法：Thread.yield()
 * 作用：让当前线程暂停，并执行其他线程。
 * 一般用于线程调度，让线程交替执行，提高CPU的利用率。
 * 注意：
 * 1. yield()方法并不一定会让线程暂停，它只是通知系统有更高优先级的线程需要运行，但并不一定会立即执行。
 * 2. yield()方法只能在线程运行过程中调用，不能在线程的构造函数或者启动前调用。
 * 3. yield()方法只能在多线程环境下使用。
 * 4. yield()方法只能在用户态中调用，不能在内核态中调用。
 * 5. yield()方法是静态方法，调用时不需要实例化Thread对象。
 * 6. yield()方法是Thread类的静态方法，可以直接通过Thread.yield()调用。
 * 7. yield()方法是Thread类的静态方法，只能在多线程环境下使用。
 * 8. yield()方法是Thread类的静态方法，不能在线程的构造函数或者启动前调用。
 * 9. yield()方法是Thread类的静态方法，不能在线程的run()方法中调用。
 * 10. yield()方法是Thread类的静态方法，不能在线程的静态方法中调用。
 * 11. yield()方法是Thread类的静态方法，不能在线程的本地方法中调用。
 * 12. yield()方法是Thread类的静态方法，不能在线程的中断中调用。
 * 13. yield()方法是Thread类的静态方法，不能在线程的等待中调用。
 * 14. yield()方法是Thread类的静态方法，不能在线程的同步中调用。
 * 15. yield()方法是Thread类的静态方法，不能在线程的阻塞中调用。
 * 16. yield()方法是Thread类的静态方法，不能在线程的超时中调用。
 * 17. yield()方法是Thread类的静态方法，不能在线程的死锁中调用。
 * 18. yield()方法是Thread类的静态方法，不能在线程的异常中调用。
 * 19. yield()方法是Thread类的静态方法，不能在线程的错误中调用。
 * 20. yield()方法是Thread类的静态方法，不能在线程的信号中调用。
 * 21. yield()方法是Thread类的静态方法，不能在线程的定时中调用。
 * 22. yield()方法是Thread类的静态方法，不能在线程的线程中调用。
 * 23. yield()方法是Thread类的静态方法，不能在线程的线程组中调用。
 * 24. yield()方法是Thread类的静态方法，不能在线程的线程栈中调用。
 * 25. yield()方法是Thread类的静态方法，不能在线程的线程状态中调用。
 * 26. yield()方法是Thread类的静态方法，不能在线程的线程优先级中调用。
 */
public class T02_Yield {
    private static final Logger log = LoggerFactory.getLogger(T02_Yield.class);

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                log.info("Thread 1: " + i);
                Thread.yield();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                log.info("Thread 2: " + i);
                Thread.yield();
            }
        });

        t1.start();
        t2.start();
    }
}
