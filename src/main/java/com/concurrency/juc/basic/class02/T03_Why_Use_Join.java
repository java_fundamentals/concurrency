package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 如果两个线程需要共享一个变量，并且需要确保该变量在两个线程之间是可见的，那么就需要使用join()方法。
 * join()方法使得一个线程等待另一个线程结束后再继续执行。
 * 例如，在一个多线程程序中，有两个线程，一个线程负责计算，另一个线程负责输出结果。
 * 为了确保输出结果的准确性，需要等待计算线程结束后再输出。
 * 这时就可以使用join()方法，让输出线程等待计算线程结束后再输出结果。
 * 代码如下：
 *
 * public static void main(String[] args) {
 *     Thread t1 = new Thread(() -> {
 *        log.info("Thread 1 started");
 *         try {
 *             TimeUnit.SECONDS.sleep(1);
 *         } catch (InterruptedException e) {
 *             throw new RuntimeException(e);
 *         }
 *         counter = 100;
 *         log.info("Thread 1 ended");
 *     }, "T1");
 *     t1.start();
 *     try {
 *         t1.join();
 *     } catch (InterruptedException e) {
 *         throw new RuntimeException(e);
 *     }
 *     log.info("结果：{}",counter);
 *     log.info("主线程结束");
 * }
 *
 * 输出结果：
 * Thread 1 started
 * Thread 1 ended
 * 结果：100
 * 主线程结束
 *
 * 说明：
 * 1. 主线程先启动，然后启动子线程t1。
 * 2. 子线程t1执行完毕后，主线程调用join()方法，等待子线程t1结束。
 * 3. 子线程t1执行完毕后，主线程输出结果。
 */
public class T03_Why_Use_Join {
    private static final Logger log = LoggerFactory.getLogger(T03_Why_Use_Join.class);

    private static int counter = 0;

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
           log.info("Thread 1 started");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            counter = 100;
            log.info("Thread 1 ended");
        }, "T1");
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("结果：{}",counter);
        log.info("主线程结束");
    }
}
