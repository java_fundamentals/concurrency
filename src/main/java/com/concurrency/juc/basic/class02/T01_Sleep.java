package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用方法：Thread.sleep()
 * 作用：让当前线程暂停执行指定的时间，以便其他线程可以执行。
 * 注意：sleep()方法是Thread类中的静态方法，调用该方法时，当前线程会暂停执行，但不会释放对象锁，因此调用sleep()方法时，其他线程仍然可以访问该对象。
 * 调用sleep()方法时，如果当前线程正在执行同步代码块，则该线程会一直处于阻塞状态，直到同步代码块执行完毕，才会从sleep()方法返回。
 * 调用sleep()方法时，如果当前线程处于等待状态（如调用Object.wait()方法），则该线程会一直处于阻塞状态，直到超时或被唤醒。
 * 调用sleep()方法时，如果当前线程是守护线程，则该线程会一直处于阻塞状态，直到超时或被停止。
 * 调用sleep()方法时，如果当前线程是最后一个非守护线程，则虚拟机会自动停止该线程。
 * 调用sleep()方法时，如果当前线程是中断状态，则会清除中断状态，并抛出InterruptedException异常。
 * 调用sleep()方法时，如果时间参数为0，则不会有任何效果，不会抛出InterruptedException异常。
 * 调用sleep()方法时，如果时间参数为负数，则会抛出IllegalArgumentException异常。
 * 调用sleep()方法时，如果时间参数为Long.MAX_VALUE，则会一直阻塞线程，直到被中断或超时。
 * 调用sleep()方法时，如果时间参数为Long.MIN_VALUE，则会一直阻塞线程，直到被中断。
 * 调用sleep()方法时，如果时间参数为其他值，则会阻塞线程，直到超时或被中断。
 * 调用sleep()方法时，如果当前线程是线程池中的线程，则不会影响线程池的正常工作。
 * 调用sleep()方法时，如果当前线程是线程组中的线程，则不会影响线程组的正常工作。
 * 调用sleep()方法时，如果当前线程是线程优先级低于Thread.MIN_PRIORITY的线程，则不会影响线程优先级的正常工作。
 * 调用sleep()方法时，如果当前线程是线程优先级高于Thread.MAX_PRIORITY的线程，则不会影响线程优先级的正常工作。
 */
public class T01_Sleep extends Thread {
    private static final Logger log = LoggerFactory.getLogger(T01_Sleep.class);

    public void run() {
        try {
            log.info("Thread is sleeping for 5 seconds");
            Thread.sleep(5000);
            log.info("Thread is awake after 5 seconds");
        } catch (InterruptedException e) {
            log.error("Thread interrupted");
        }
    }
    public static void main(String[] args) {
        new T01_Sleep().start();
    }
}
