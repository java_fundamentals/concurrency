package com.concurrency.juc.basic.class02;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 守护线程
 * 守护线程是一种特殊的线程，它主要用于为整个JVM提供服务，并且在JVM退出时会自动结束。
 * 守护线程的特点是：守护线程不会阻止JVM的退出，并且守护线程不能持有任何需要关闭的资源（如文件、数据库连接等）。
 * 一般情况下，守护线程都是由JVM创建的，例如垃圾回收线程就是一个守护线程。
 * 我们可以通过Thread.setDaemon(true)方法将线程设置为守护线程。
 * 注意：
 * 1. 守护线程只能在启动线程时设置，不能在线程运行时设置。
 * 2. 主线程结束时，守护线程也会结束。
 * 3. 守护线程不能持有需要关闭的资源，例如打开的文件等。
 * 4. 守护线程在程序结束时会自动结束，而非正常线程。
 * 5. 守护线程通常用于程序的后台运行，例如垃圾回收线程。
 * 6. 守护线程的优先级通常较低，因为它不参与程序的执行，只提供服务。
 * 7. 守护线程的执行顺序通常是不确定的，因为它没有固定的优先级。
 *
 * 守护线程的用途：
 * 1. 垃圾回收线程：用于回收程序运行过程中产生的垃圾内存。
 * 2. 后台监控线程：用于监控程序的运行状态，例如性能监控、内存监控等。
 * 3. 服务器线程：用于提供服务器的运行环境，例如网络服务、数据库服务等。
 * 4. 定时任务线程：用于执行定时任务，例如每天定时执行的任务。
 * 5. 日志线程：用于记录程序的运行日志。
 * 6. 其他：一些特殊的需求。
 *
 * 优点：
 * 1. 提高程序的响应速度。
 * 2. 节省系统资源。
 * 3. 防止程序意外退出。
 * 4. 简化程序的编写。
 *
 * 缺点：
 * 1. 守护线程不能执行一些需要用户交互的操作。
 * 2. 守护线程不能执行一些需要长时间运行的操作。
 * 3. 守护线程的异常处理比较困难。
 * 4. 守护线程的执行顺序是不确定的，因为它没有固定的优先级。
 * 5. 守护线程通常用于程序的后台运行，而不是主线程。
 * 6. 守护线程的优先级通常较低，因为它不参与程序的执行，只提供服务。
 */
public class T06_Daemon {
    private static final Logger log = LoggerFactory.getLogger(T06_Daemon.class);

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("线程还在运行");
            }
        }, "t");
        // 设置为守护线程
        t.setDaemon(true);
        t.start();
        Thread.sleep(2000);
        log.info("主线程结束");
        // 主线程结束，t线程也会结束
        // 守护线程会随着主线程的结束而结束
        // 主线程结束，t线程还在运行，但是主线程结束，t线程也会结束
    }
}
