package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 打断sleep,wait,join方法
 * 1.interrupt()方法可以打断正在运行的线程，但是该方法是异步操作，并不能保证一定能打断正在运行的线程。
 *
 */
public class T04_Interrupt02 {
    private static final Logger log = LoggerFactory.getLogger(T04_Interrupt02.class);

    public static void main(String[] args) {
        Thread t = new Thread(()-> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                log.info("Thread interrupted");
            }
        },"线程1");
        t.start();
        log.info("main thread is waiting for 1 second");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            log.info("main thread interrupted");
        }
        t.interrupt();
        log.info("main thread is done,thread status is {}",t.isInterrupted());
    }
}
