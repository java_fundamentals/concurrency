package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 * 线程状态
 * NEW - 新创建的线程，尚未启动
 * RUNNABLE - 运行中的线程，正在执行run()方法的Java代码
 * BLOCKED - 线程被阻塞，因为它试图获取一个由其他线程持有的对象锁
 * WAITING - 线程处于阻塞状态，正在等待另一个线程执行特定操作
 * TIMED_WAITING - 线程处于阻塞状态，正在等待另一个线程执行特定操作，但是指定的等待时间已过
 * TERMINATED - 线程已终止，没有运行的线程
 * 线程状态的判断：
 * 1. 通过Thread.getState()方法获取线程的状态
 * 2. 通过Thread.getState()方法返回的枚举类型来判断线程的状态
 * 3. 通过Thread.getState()方法返回的枚举类型的值来判断线程的状态，具体如下：
 *    - NEW: 新创建的线程，尚未启动
 *    - RUNNABLE: 运行中的线程，正在执行run()方法的Java代码
 *    - BLOCKED: 线程被阻塞，因为它试图获取一个由其他线程持有的对象锁
 *    - WAITING: 线程处于阻塞状态，正在等待另一个线程执行特定操作
 *    - TIMED_WAITING: 线程处于阻塞状态，正在等待另一个线程执行特定操作，但是指定的等待时间已过
 *    - TERMINATED: 线程已终止，没有运行的线程
 * 4. 通过Thread.getState()方法返回的枚举类型的值来判断线程的状态，具体如下：
 *    - NEW: 0
 *    - RUNNABLE: 1
 *    - BLOCKED: 2
 *    - WAITING: 3
 *    - TIMED_WAITING: 4
 *    - TERMINATED: 6
 * 5. 通过Thread.getState()方法返回的枚举类型的值来判断线程的状态，具体如下：
 *    - NEW: "NEW"
 *    - RUNNABLE: "RUNNABLE"
 *    - BLOCKED: "BLOCKED"
 *    - WAITING: "WAITING"
 *    - TIMED_WAITING: "TIMED_WAITING"
 *    - TERMINATED: "TERMINATED"
 */
public class T07_ThreadState {
    public static final Logger log = LoggerFactory.getLogger(T07_ThreadState.class);

    public static void main(String[] args) throws InterruptedException {
        /**
         * NEW - 新创建的线程，尚未启动
         */
        Thread t1 = new Thread("t1"){
            @Override
            public void run() {
                log.info("t1 is running");
            }
        };

        /**
         * RUNNABLE - 线程正在运行，可执行代码
         */
        Thread t2 = new Thread("t2"){
            @Override
            public void run() {
                while (true) {

                }
            }
        };
        t2.start();

        /**
         * TERMINATED - 线程已终止，没有运行的线程
         */
        Thread t3 = new Thread("t3"){
            @Override
            public void run() {
                log.info("t3 is running");
            }
        };
        t3.start();

        /**
         * TIMED_WAITING - 线程处于阻塞状态，正在等待另一个线程执行特定操作
         */
        Thread t4 = new Thread("t4"){
            @Override
            public void run() {
                synchronized (T07_ThreadState.class) {
                    try {
                        TimeUnit.SECONDS.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t4.start();

        /**
         * WAITING - 线程处于阻塞状态，正在等待另一个线程执行特定操作
         */
        Thread t5 = new Thread("t5"){
            @Override
            public void run() {
                try {
                    t2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t5.start();

        /**
         * BLOCKED - 线程被阻塞，因为它试图获取一个由其他线程持有的对象锁
         */
        Thread t6 = new Thread("t6"){
            @Override
            public void run() {
                synchronized (T07_ThreadState.class) {
                    try {
                        TimeUnit.SECONDS.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t6.start();

        // 线程状态
        log.info("t1 state: {}", t1.getState());
        log.info("t2 state: {}", t2.getState());
        log.info("t3 state: {}", t3.getState());
        log.info("t4 state: {}", t4.getState());
        log.info("t5 state: {}", t5.getState());
        log.info("t6 state: {}", t6.getState());
    }
}
