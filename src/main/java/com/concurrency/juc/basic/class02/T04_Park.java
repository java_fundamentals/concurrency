package com.concurrency.juc.basic.class02;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport.park() 方法的使用
 *
 * 调用LockSupport.park()方法的线程，会一直阻塞，直到被其他线程调用LockSupport.unpark(thread)方法唤醒。
 * 调用LockSupport.unpark(thread)方法的线程，会将thread线程唤醒，继续执行。
 * 调用LockSupport.park()方法的线程，可以设置超时时间，如果超时时间内没有被唤醒，则线程会自动唤醒。
 * 调用LockSupport.park()方法的线程，可以设置阻塞条件，只有满足阻塞条件时，才会被唤醒。
 * 调用LockSupport.park()方法的线程，可以设置唤醒动作，在唤醒时，会执行指定的动作。
 * 调用LockSupport.park()方法的线程，可以设置阻塞优先级，在满足阻塞条件时，会按照优先级顺序进行唤醒。
 *
 * 注意：
 * 1. LockSupport.park()方法不会释放锁。
 * 2. LockSupport.park()方法可以被中断。
 * 3. LockSupport.park()方法可以被限时唤醒。
 * 4. LockSupport.park()方法可以设置阻塞条件。
 * 5. LockSupport.park()方法可以设置唤醒动作。
 * 6. LockSupport.park()方法可以设置阻塞优先级。
 * 7. LockSupport.park()方法可以设置超时时间。
 * 8. LockSupport.park()方法可以设置阻塞线程。
 *
 * 调用LockSupport.park()方法的线程，如果被中断，则会抛出InterruptedException异常。
 * interrupt方法可以中断LockSupport.park()方法，但是不会影响LockSupport.park()方法的阻塞。
 *
 * LockSupport.park()方法的使用场景：
 * 1. 线程间通信，可以用LockSupport.unpark(thread)方法唤醒线程。
 * 2. 线程等待，可以用LockSupport.park()方法阻塞线程。
 * 3. 线程唤醒，可以用LockSupport.unpark(thread)方法唤醒线程。
 * 4. 线程限时等待，可以用LockSupport.parkNanos(thread, timeout)方法限时唤醒线程。
 * 5. 线程限时唤醒，可以用LockSupport.parkNanos(thread, timeout)方法限时唤醒线程。
 * 6. 线程阻塞，可以用LockSupport.park()方法阻塞线程。
 * 7. 线程阻塞，可以用LockSupport.park()方法阻塞线程，并设置阻塞条件。
 * 8. 线程阻塞，可以用LockSupport.park()方法阻塞线程，并设置唤醒动作。
 * 9. 线程阻塞，可以用LockSupport.park()方法阻塞线程，并设置阻塞优先级。
 * 10. 线程阻塞，可以用LockSupport.park()方法阻塞线程，并设置超时时间。
 * 11. 线程阻塞，可以用LockSupport.park()方法阻塞线程，并设置阻塞线程。
 */
public class T04_Park {
    private static final Logger log = LoggerFactory.getLogger(T04_Park.class);

    public static void main(String[] args) {
        park();
    }

    public static void park() {
        Thread thread = new Thread(() -> {
            log.info("Thread {} parking", Thread.currentThread().getName());
            // 阻塞当前线程，并释放锁
            LockSupport.park();
            log.info("Thread {} parked", Thread.currentThread().getName());
            log.info("Thread {} 状态为：{}", Thread.currentThread().getName(), Thread.currentThread().isInterrupted());
            // 阻塞结束后，线程会尝试获取锁，如果获取不到，则进入等待状态
            // 获取到锁的线程会继续执行
            // 线程获取到锁后，可以继续执行，不会被阻塞
            // 线程获取到锁后，可以调用LockSupport.unpark(thread)方法，将线程唤醒，继续执行

            // 线程获取到锁后，可以调用LockSupport.unpark()方法，将当前线程唤醒，继续执行
            LockSupport.unpark(Thread.currentThread());
            log.info("Thread {} unparked", Thread.currentThread().getName());

            log.info("Thread {} 状态为：{}", Thread.currentThread().getName(), Thread.currentThread().isInterrupted());
        }, "park-thread");
        // 线程启动后，不会自动执行，需要调用start()方法
        thread.start();

        // 主线程休眠1秒，等待线程执行
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 主线程调用LockSupport.unpark(thread)方法，将线程唤醒，继续执行
//        LockSupport.unpark(thread);
//        log.info("Main thread unparked");
        thread.interrupt();
    }
}
