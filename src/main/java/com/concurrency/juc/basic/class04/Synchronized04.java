package com.concurrency.juc.basic.class04;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author liuc
 * @date 2024-07-10 21:02
 */
public class Synchronized04 {
    private static final Logger log = LoggerFactory.getLogger(Synchronized04.class);

    /**
     * 方法说明：
     * 1. 两个线程，一个线程调用a()方法，另一个线程调用b()方法
     * 2. 两个方法都加了synchronized关键字，保证同一时刻只有一个线程执行该方法
     * 3. 两个线程都执行完毕后，程序结束
     * 4. 输出结果：
     * t2 begin
     * t1 begin
     * 2
     * 睡眠1秒钟
     * 1
     * 或者
     * t1 begin
     * t2 begin
     * 2
     * 睡眠1秒钟
     * 1
     */
    public static void main(String[] args) {
        Number4 numbera = new Number4();
        Number4 numberb = new Number4();
        new Thread(() -> {
            log.info("t1 begin");
            numbera.a();
        }, "t1").start();

        new Thread(() -> {
            log.info("t2 begin");
            numberb.b();
        },"t2").start();
    }
}

class Number4 {
    private static final Logger log = LoggerFactory.getLogger(Number.class);
    public synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("1");
    }

    public synchronized void b() {
        log.info("2");
    }

}
