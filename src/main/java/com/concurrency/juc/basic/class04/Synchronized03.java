package com.concurrency.juc.basic.class04;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author liuc
 * @date 2024-07-10 21:02
 */
public class Synchronized03 {
    private static final Logger log = LoggerFactory.getLogger(Synchronized03.class);
    public static void main(String[] args) {
        Number3 number = new Number3();
        new Thread(() -> {
            log.info("t1 begin");
            number.a();
        }, "t1").start();

        new Thread(() -> {
            log.info("t2 begin");
            number.b();
        },"t2").start();


        new Thread(() -> {
            log.info("t3 begin");
            number.c();
        }, "t3").start();
    }
}

/**
 * 代码说明：
 * 三个线程分别调用Number3对象的a()、b()、c()方法，由于Number3类中没有使用synchronized关键字，因此三个线程的执行顺序是随机的。
 * 但是，由于a()和b()方法使用了synchronized关键字，因此它们是同步的，即一个线程执行完a()方法后，另一个线程才能执行b()方法。
 * 因此，输出结果可能是：
 * t1 begin
 * t2 begin
 * t3 begin
 * 3
 * 睡眠1秒钟
 * 1
 * 2
 * 或者：
 * t2 begin
 * t3 begin
 * t1 begin
 * 2
 * 3
 * 睡眠1秒钟
 * 1
 * 或者：
 * t1 begin
 * t2 begin
 * t3 begin
 * 3
 * 睡眠1秒钟
 * 1
 * 2
 * 上述输出结果中，t1、t2、t3的执行顺序是随机的，但是a()和b()方法是同步的，即一个线程执行完a()方法后，另一个线程才能执行b()方法。
 * 因此，输出结果中，数字1和2的输出顺序是确定的，而数字3的输出顺序则是随机的。
 *
 */
class Number3 {
    private static final Logger log = LoggerFactory.getLogger(Number.class);
    public synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("1");
    }

    public synchronized void b() {
        log.info("2");
    }

    public void c() {
        log.info("3");
    }
}
