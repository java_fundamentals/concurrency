package com.concurrency.juc.basic.class04;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author liuc
 * @date 2024-07-10 21:02
 */
public class Synchronized02 {
    private static final Logger log = LoggerFactory.getLogger(Synchronized02.class);
    public static void main(String[] args) {
        Number2 number = new Number2();
        new Thread(() -> {
            log.info("t1 begin");
            number.a();
        }, "t1").start();

        new Thread(() -> {
            log.info("t2 begin");
            number.b();
        },"t2").start();
    }
}

/**
 * Number类中使用了synchronized关键字，表示该方法是同步方法，同一时刻只能有一个线程执行该方法。
 * 因此，两个线程只能交替执行a()和b()方法，不会出现同时执行a()和b()的情况。
 * 输出结果：
 *  t1 begin
 *  t2 begin
 *  1
 *  睡眠1秒钟
 *  2
 *  或者
 *  t2 begin
 *  t1 begin
 *  2
 *  睡眠1秒钟
 *  1
 *  此时synchronized关键字锁的都是this对象，因此两个线程的执行顺序是随机的。
 */
class Number2 {
    private static final Logger log = LoggerFactory.getLogger(Number.class);
    public synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("1");
    }

    public synchronized void b() {
        log.info("2");
    }
}
