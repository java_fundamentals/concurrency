package com.concurrency.juc.basic.class03;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 解决共享资源竞争问题
 * 两个线程同时对一个共享变量进行操作，可能会导致数据不一致的问题。
 * 解决方案：
 * 1. 加锁：使用同步机制，确保同一时刻只有一个线程对共享资源进行操作。
 * 2. 原子操作：使用原子操作，确保对共享资源的操作是不可分割的。
 * 3. 加锁粒度：降低锁的粒度，减少锁竞争。
 * 4. 读写分离：读操作和写操作分离，读操作不加锁，写操作加锁。
 * 5. 延迟初始化：将共享资源的初始化推迟到需要使用时再进行，避免资源竞争。
 * 6. 线程安全类：使用线程安全类，避免手动加锁。
 * 7. 复制变量：在多个线程之间复制变量，避免共享变量的竞争。
 * 8. 线程池：使用线程池，将线程的创建和管理交给线程池，避免线程的频繁创建和销毁。
 * 9. 线程同步：使用线程同步机制，如信号量、互斥锁等，确保多个线程对共享资源的操作顺序一致。
 * 10. 线程等待：使用线程等待机制，如等待队列、条件变量等，确保线程按照指定的顺序执行。
 * 11. 线程间通信：使用线程间通信机制，如共享变量、消息队列等，确保线程间通信的效率。
 * 12. 并发容器：使用并发容器，如ConcurrentHashMap、ConcurrentLinkedQueue等，确保容器操作的原子性。
 *
 * @author liuc
 * @date 2024-05-21 22:18
 */
public class T01_SolveSharedProblem01 {
    private static final Logger log = LoggerFactory.getLogger(T01_SolveSharedProblem01.class);

    static int count = 0;
    static final Object obj = new Object();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                synchronized (obj) {
                    count++;
                }
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                synchronized (obj) {
                    count--;
                }
            }
        }, "t2");

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("count: {}", count);
    }
}
