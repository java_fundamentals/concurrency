package com.concurrency.juc.basic.class03;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 线程资源共享带来的问题
 * 两个线程对初始值为0的变量count进行++和--操作，结果是0吗？
 * 答案是不一定，因为两个线程操作共享变量count时，可能存在数据竞争，导致结果不是0。
 * 解决数据竞争的方法有两种：
 * 1. 加锁：通过锁机制，确保同一时刻只有一个线程对共享变量进行操作。
 * 2. 原子操作：通过原子操作，确保对共享变量的操作是原子性的，即不可分割。
 * @author liuc
 * @date 2024-05-20 22:33
 */
public class T01_SharedProblem {
    private static final Logger log = LoggerFactory.getLogger(T01_SharedProblem.class);

    static int count = 0;

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                count++;
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                count--;
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("count: {}", count);
    }
}
